package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import javax.swing.JList;
import javax.swing.JButton;

public class PagamentoPresencial {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PagamentoPresencial window = new PagamentoPresencial();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PagamentoPresencial() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 705, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(12, 23, 693, 448);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblMesa = new JLabel("Mesa");
		lblMesa.setBounds(32, 70, 70, 15);
		panel.add(lblMesa);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(116, 65, 113, 24);
		panel.add(comboBox);
		
		JList list = new JList();
		list.setBounds(32, 133, 634, 285);
		panel.add(list);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(458, 65, 113, 24);
		panel.add(comboBox_1);
		
		JLabel lblPagamento = new JLabel("Pagamento");
		lblPagamento.setBounds(340, 70, 104, 15);
		panel.add(lblPagamento);
		
		JButton btnOkMesa = new JButton("OK");
		btnOkMesa.setBounds(235, 65, 54, 25);
		panel.add(btnOkMesa);
		
		JButton btnPagamento = new JButton("OK");
		btnPagamento.setBounds(583, 65, 54, 25);
		panel.add(btnPagamento);
	}

	public void setVisible(boolean b) {
		// TODO Auto-generated method stub
		frame.setVisible(b);
	}
}
