package View;

import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import java.io.IOException;

import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import javax.swing.JList;

import Controller.CadastrarEstoque;
import Controller.CadastrarMesa;
import Model.Bebida;
import Model.Mesa;
import Model.PedidoPresencial;
import Model.Pratos;
import Model.Produtos;
import Model.Sobremesa;

import javax.swing.JTextField;
import javax.swing.JEditorPane;
import java.awt.Color;

public class NovoPedidoPresencial {

	private JFrame frame;
	private JTextField txtMesa;
	private JTextField txtPrato;
	private JTextField txtBebida;
	private JTextField txtSobremesa;
	private JTextField txQtPrato;
	private JTextField txQtBebida;
	private JTextField txQtSobre;
	// Melhor Funcionamento do Cadastro
	public Produtos produtos[] = new Produtos[200]; 
	public int quantidadePedida[] = new int[200];
	public int contador;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NovoPedidoPresencial window = new NovoPedidoPresencial();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NovoPedidoPresencial() {
		initialize();
		produtos = new Produtos[200];
		quantidadePedida = new int [200];
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 715, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 12, 715, 467);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblMesa = new JLabel("Mesa");
		lblMesa.setBounds(12, 64, 38, 15);
		panel.add(lblMesa);
		
		JLabel lblPratos = new JLabel("Pratos");
		lblPratos.setBounds(12, 139, 83, 15);
		panel.add(lblPratos);
		
		JLabel lblBebidas = new JLabel("Bebidas");
		lblBebidas.setBounds(12, 219, 62, 15);
		panel.add(lblBebidas);
		
		JLabel lblSobremesa = new JLabel("Sobremesa");
		lblSobremesa.setBounds(12, 302, 83, 15);
		panel.add(lblSobremesa);
		
		JButton btnOkPratos = new JButton("OK");
		btnOkPratos.setBounds(437, 129, 54, 25);
		panel.add(btnOkPratos);
		
		JButton btnOkBebidas = new JButton("OK");
		btnOkBebidas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CadastrarEstoque cadastro = new CadastrarEstoque();
				String codigo = txtPrato.getText();
				String quantidade = txQtPrato.getText();
				
				try {
					if(cadastro.qtMinimaPrato(codigo, quantidade) == true){			
						
						Bebida bebida = new Bebida();
						 bebida = cadastro.receberBebida(codigo, quantidade);
						
						 produtos[contador] = bebida;
						 contador += 1;
					}
					else{
						JOptionPane.showMessageDialog(null, "Quantidade Minima No Estoque Atingida! \n NÃO SIRVA Essa Comida!");
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnOkBebidas.setBounds(437, 209, 54, 25);
		panel.add(btnOkBebidas);
		
		JButton btnOkSobre = new JButton("OK");
		btnOkSobre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CadastrarEstoque cadastro = new CadastrarEstoque();
				String codigo = txtPrato.getText();
				String quantidade = txQtPrato.getText();
				
				try {
					if(cadastro.qtMinimaItem(codigo, quantidade) == true){			
						
						Sobremesa sobre = new Sobremesa();
						 sobre = cadastro.receberSobremesa(codigo, quantidade);
						
						 produtos[contador] = sobre;
						 contador += 1;
					}
					else{
						JOptionPane.showMessageDialog(null, "Quantidade Minima No Estoque Atingida! \n NÃO SIRVA Essa Comida!");
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnOkSobre.setBounds(437, 292, 54, 25);
		panel.add(btnOkSobre);
		
		JLabel label = new JLabel("Qt.");
		label.setBounds(328, 129, 38, 15);
		panel.add(label);
		
		JLabel label_1 = new JLabel("Qt.");
		label_1.setBounds(328, 209, 38, 15);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("Qt.");
		label_2.setBounds(328, 292, 38, 15);
		panel.add(label_2);
		
		txtMesa = new JTextField();
		txtMesa.setBounds(117, 62, 131, 19);
		panel.add(txtMesa);
		txtMesa.setColumns(10);
		
		txtPrato = new JTextField();
		txtPrato.setColumns(10);
		txtPrato.setBounds(192, 129, 107, 19);
		panel.add(txtPrato);
		
		txtBebida = new JTextField();
		txtBebida.setColumns(10);
		txtBebida.setBounds(192, 209, 107, 19);
		panel.add(txtBebida);
		
		txtSobremesa = new JTextField();
		txtSobremesa.setColumns(10);
		txtSobremesa.setBounds(192, 292, 107, 19);
		panel.add(txtSobremesa);
		
		txQtPrato = new JTextField();
		txQtPrato.setColumns(10);
		txQtPrato.setBounds(367, 129, 46, 19);
		panel.add(txQtPrato);
		
		txQtBebida = new JTextField();
		txQtBebida.setColumns(10);
		txQtBebida.setBounds(367, 209, 46, 19);
		panel.add(txQtBebida);
		
		txQtSobre = new JTextField();
		txQtSobre.setColumns(10);
		txQtSobre.setBounds(367, 295, 46, 19);
		panel.add(txQtSobre);
		
		JButton btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(286, 359, 107, 25);
		panel.add(btnConfirmar);
		

		
		JLabel label_3 = new JLabel("Tô com Fome, Quero Mais!");
		label_3.setBounds(507, 410, 185, 15);
		panel.add(label_3);
		
		JLabel lblConfirmaPrato = new JLabel("");
		lblConfirmaPrato.setBounds(485, 139, 70, 15);
		panel.add(lblConfirmaPrato);
		
		JLabel lblConfirmaBebida = new JLabel("");
		lblConfirmaBebida.setBounds(485, 243, 70, 15);
		panel.add(lblConfirmaBebida);
		
		JLabel lblConfirmaSobre = new JLabel("");
		lblConfirmaSobre.setBounds(485, 302, 70, 15);
		panel.add(lblConfirmaSobre);
		
		JLabel lblCdigo = new JLabel("Código");
		lblCdigo.setBounds(113, 134, 59, 15);
		panel.add(lblCdigo);
		
		JLabel lblCdigo_1 = new JLabel("Código");
		lblCdigo_1.setBounds(113, 209, 59, 15);
		panel.add(lblCdigo_1);
		
		JLabel lblCdigo_2 = new JLabel("Código");
		lblCdigo_2.setBounds(113, 292, 59, 15);
		panel.add(lblCdigo_2);
		
		JEditorPane dtrpnDigiteONmero = new JEditorPane();
		dtrpnDigiteONmero.setText("Digite o número negativo na quantidade para retirar Itens.\n\nCada vez que Clica no Botão OK de cada Item\nAdiciona um ao Pedido.\n\nClique Confirmar quando Acabar de Adicionar Todos.");
		dtrpnDigiteONmero.setBackground(UIManager.getColor("Button.background"));
		dtrpnDigiteONmero.setBounds(286, 12, 413, 96);
		panel.add(dtrpnDigiteONmero);
		
		final JTextPane obs = new JTextPane();
		obs.setBackground(Color.WHITE);
		obs.setBounds(507, 166, 192, 218);
		panel.add(obs);
		
		JLabel lblObservaes = new JLabel("Observações");
		lblObservaes.setBounds(553, 139, 95, 15);
		panel.add(lblObservaes);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnUsurio = new JMenu("Usuário");
		menuBar.add(mnUsurio);
		
		JMenuItem mntmEstoque = new JMenuItem("Estoque");
		mnUsurio.add(mntmEstoque);
		
		JMenuItem mntmPedidos = new JMenuItem("Pedidos");
		mnUsurio.add(mntmPedidos);
		
		JMenuItem mntmMenuPrincipal = new JMenuItem("Menu Principal");
		mnUsurio.add(mntmMenuPrincipal);
		
		JMenuItem mntmSair = new JMenuItem("Sair");
		mnUsurio.add(mntmSair);
		
		JMenu mnArquivo = new JMenu("Arquivo");
		menuBar.add(mnArquivo);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnArquivo.add(mntmCopiar);
		
		JMenuItem mntmRecortar = new JMenuItem("Recortar");
		mnArquivo.add(mntmRecortar);
		
		JMenuItem mntmColar = new JMenuItem("Colar");
		mnArquivo.add(mntmColar);

		// Botao Ok Pratos
		btnOkPratos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CadastrarEstoque cadastro = new CadastrarEstoque();
				String codigo = txtPrato.getText();
				String quantidade = txQtPrato.getText();
				
				try {
					if(cadastro.qtMinimaPrato(codigo, quantidade) == true){			
						
						Pratos prato = new Pratos();
						 prato = cadastro.receberPrato(codigo, quantidade);
						
						 produtos[contador] = prato;
						 contador += 1;

						 
					}
					else{
						JOptionPane.showMessageDialog(null, "Quantidade Minima No Estoque Atingida! \n NÃO SIRVA Essa Comida!");
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		
		// Confirmar Pedido
		
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				PedidoPresencial pedido = new PedidoPresencial(produtos, quantidadePedida);
				pedido.setMesa(Integer.valueOf((txtMesa.getText())));
				pedido.setPago(false);
				String []auxiliar = new String[1];
				String aux = obs.getText();
				auxiliar[0] = aux;
				pedido.setObservacao(auxiliar);
				CadastrarMesa ircadastro = new CadastrarMesa();
				try {
					ircadastro.salvarPedido(pedido);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				frame.dispose();
				InterfacePedidos ir = new InterfacePedidos();
				ir.setVisible(true);
				
				
			}
		});
	}

	public void setVisible(boolean b) {
		// TODO Auto-generated method stub
		frame.setVisible(b);
	}
}
