package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class InterfacePedidos {

	private JFrame frmPedidosT;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfacePedidos window = new InterfacePedidos();
					window.frmPedidosT.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfacePedidos() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPedidosT = new JFrame();
		frmPedidosT.setTitle("Pedidos - Tô com Fome");
		frmPedidosT.setBounds(100, 100, 700, 500);
		frmPedidosT.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPedidosT.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(12, 0, 723, 439);
		frmPedidosT.getContentPane().add(panel);
		panel.setLayout(null);
		
		//Telefone
		JButton btnTelefone = new JButton("Telefone");
		btnTelefone.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				frmPedidosT.dispose();
				InterfaceTelefone ir = new InterfaceTelefone();
				ir.setVisible(true);
			}
		});
		btnTelefone.setBounds(254, 191, 172, 25);
		panel.add(btnTelefone);
		
		JButton btnPresencial = new JButton("Presencial");
		btnPresencial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmPedidosT.dispose();
				InterfacePedidoPresencial ir = new InterfacePedidoPresencial();
				ir.setVisible(true);
			}
		});
		btnPresencial.setBounds(60, 191, 172, 25);
		panel.add(btnPresencial);
		
		JLabel label = new JLabel("Tô com Fome, Quero Mais!");
		label.setBounds(443, 412, 185, 15);
		panel.add(label);
		
		JButton btnPagamento = new JButton("Pagamento");
		btnPagamento.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				frmPedidosT.dispose();
				PagamentoPresencial ir = new PagamentoPresencial();
				ir.setVisible(true);
			}
		});
		btnPagamento.setBounds(454, 191, 172, 25);
		panel.add(btnPagamento);
		
		JMenuBar menuBar = new JMenuBar();
		frmPedidosT.setJMenuBar(menuBar);
		
		JMenu mnUsurio = new JMenu("Usuário");
		menuBar.add(mnUsurio);
		
		JMenuItem mntmEstoque = new JMenuItem("Estoque");
		mntmEstoque.addMouseListener(new MouseAdapter() {

		
		});
		mnUsurio.add(mntmEstoque);
		
		JMenuItem mntmMenuPrincipal = new JMenuItem("Menu Principal");
		mntmMenuPrincipal.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmPedidosT.dispose();
				InterfaceMenu ir = new InterfaceMenu();
				ir.setVisible(true);
			}
		});
		mnUsurio.add(mntmMenuPrincipal);
		
		JMenuItem mntmSair = new JMenuItem("Sair");
		mntmSair.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmPedidosT.dispose();
				InterfaceLogin ir = new InterfaceLogin();
				ir.setVisible(true);
			}
		});
		mnUsurio.add(mntmSair);
		
		JMenuItem mntmCardpio = new JMenuItem("Cardápio");
		mntmCardpio.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmPedidosT.dispose();
				InterfaceCardapio ir = new InterfaceCardapio();
				ir.setVisible(true);
			}
		});
		menuBar.add(mntmCardpio);
	}

	public void setVisible(boolean b) {
		frmPedidosT.setVisible(true);
		
	}
}
