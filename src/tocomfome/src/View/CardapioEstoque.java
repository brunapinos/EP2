package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JEditorPane;

import Controller.CadastrarEstoque;

public class CardapioEstoque {

	private JFrame frmCardapio;
	private JTextField txtTipo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CardapioEstoque window = new CardapioEstoque();
					window.frmCardapio.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CardapioEstoque() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCardapio = new JFrame();
		frmCardapio.setTitle("Cardapio - Tô com Fome");
		frmCardapio.setBounds(100, 100, 700, 500);
		frmCardapio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCardapio.getContentPane().setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setToolTipText("");
		menuBar.setBounds(0, 0, 450, 21);
		frmCardapio.getContentPane().add(menuBar);
		
		JMenu mnUsuario = new JMenu("Usuario");
		menuBar.add(mnUsuario);
		
		JMenuItem mntmEstoque = new JMenuItem("Estoque");
		mnUsuario.add(mntmEstoque);
		
		JMenuItem mntmMenuPrincipal = new JMenuItem("Menu Principal");
		mnUsuario.add(mntmMenuPrincipal);
		
		JMenuItem mntmSair = new JMenuItem("Sair");
		mnUsuario.add(mntmSair);
		
		JMenu mnArquivo = new JMenu("Arquivo");
		menuBar.add(mnArquivo);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnArquivo.add(mntmCopiar);
		
		JMenuItem mntmRecortar = new JMenuItem("Recortar");
		mnArquivo.add(mntmRecortar);
		
		JMenuItem mntmColar = new JMenuItem("Colar");
		mnArquivo.add(mntmColar);
		
		JMenuItem mntmCardpio = new JMenuItem("Cardápio");
		menuBar.add(mntmCardpio);
		
		JLabel lblBusca = new JLabel("Tipo");
		lblBusca.setHorizontalAlignment(SwingConstants.CENTER);
		lblBusca.setFont(new Font("Dialog", Font.BOLD, 16));
		lblBusca.setBounds(-28, 33, 205, 44);
		frmCardapio.getContentPane().add(lblBusca);
		
		JLabel label_1 = new JLabel("Tô com Fome, Quero Mais!");
		label_1.setBounds(479, 429, 196, 15);
		frmCardapio.getContentPane().add(label_1);
		
		final JEditorPane editorPane = new JEditorPane();
		editorPane.setEditable(true);
		editorPane.setBounds(12, 84, 676, 360);
		frmCardapio.getContentPane().add(editorPane);
		
		txtTipo = new JTextField();
		txtTipo.setBounds(144, 47, 165, 19);
		frmCardapio.getContentPane().add(txtTipo);
		txtTipo.setColumns(10);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				CadastrarEstoque cadastro = new CadastrarEstoque();
				if (txtTipo.equals("sobremesa")){
					String []vetor;
					try {
						cadastro.mostrarBebida();
	 
						}
					 catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

			}
		});
		btnOk.setBounds(333, 33, 117, 25);
		frmCardapio.getContentPane().add(btnOk);
	}
}
