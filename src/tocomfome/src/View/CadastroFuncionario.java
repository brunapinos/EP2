package View;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JButton;

import Controller.CadastrarFuncionario;
import Model.Funcionario;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CadastroFuncionario {

	private JFrame frmCadastroFuncionario;
	private JPasswordField confirmaSenha;
	private JPasswordField areaSenha;
	private JTextField areaUsuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroFuncionario window = new CadastroFuncionario();
					window.frmCadastroFuncionario.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CadastroFuncionario() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCadastroFuncionario = new JFrame();
		frmCadastroFuncionario.setTitle("Cadastro Funcionario - Tô com Fome");
		frmCadastroFuncionario.setBounds(100, 100, 705, 500);
		frmCadastroFuncionario.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCadastroFuncionario.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(12, 12, 681, 448);
		frmCadastroFuncionario.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNomeDeUsurio = new JLabel("Nome de Usuário");
		lblNomeDeUsurio.setBounds(190, 132, 154, 15);
		panel.add(lblNomeDeUsurio);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setBounds(190, 202, 154, 15);
		panel.add(lblSenha);
		
		JLabel lblConfirmarSenha = new JLabel("Confirmar Senha");
		lblConfirmarSenha.setBounds(190, 274, 154, 15);
		panel.add(lblConfirmarSenha);
		
		confirmaSenha = new JPasswordField();
		confirmaSenha.setBounds(362, 272, 127, 19);
		panel.add(confirmaSenha);
		
		areaSenha = new JPasswordField();
		areaSenha.setBounds(362, 200, 127, 19);
		panel.add(areaSenha);
		
		areaUsuario = new JTextField();
		areaUsuario.setBounds(362, 130, 127, 19);
		panel.add(areaUsuario);
		areaUsuario.setColumns(10);
		
		// Efetuar Cadastro
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addMouseListener(new MouseAdapter() {
			@SuppressWarnings("deprecation")
			@Override
			public void mouseClicked(MouseEvent e) {
				
				if(areaSenha.getText().equals(confirmaSenha.getText())) {
					// senha
						Funcionario funcionario = new Funcionario();
						
						funcionario.setLogin(areaUsuario.getText());
						funcionario.setSenha(areaSenha.getText());
						
						CadastrarFuncionario cadastro = new CadastrarFuncionario();

							try {
								cadastro.criacaoArquivo(funcionario);
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							JOptionPane.showMessageDialog(null,"Cadastro Feito com Sucesso!");
							frmCadastroFuncionario.dispose();
							InterfaceLogin irLogin = new InterfaceLogin();
								irLogin.setVisible(true);
				}	
				
					else {// limpa entrada da senha
							

						JOptionPane.showMessageDialog(null,"Senhas não Compativeis! Tente novamente.");

									areaSenha.setText("");
									confirmaSenha.setText("");

					// limpa entrada da senha
					}
			}
		});

		btnCadastrar.setBounds(156, 348, 117, 25);
		panel.add(btnCadastrar);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.setBounds(410, 348, 117, 25);
		panel.add(btnVoltar);
	}

	public void setVisible(boolean b) {
	
		frmCadastroFuncionario.setVisible(true);
	}

}
