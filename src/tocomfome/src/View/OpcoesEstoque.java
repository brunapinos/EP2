package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class OpcoesEstoque {

	private JFrame frmEstoque;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OpcoesEstoque window = new OpcoesEstoque();
					window.frmEstoque.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public OpcoesEstoque() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEstoque = new JFrame();
		frmEstoque.setTitle("Estoque - Tô com Fome");
		frmEstoque.setBounds(100, 100, 700, 500);
		frmEstoque.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frmEstoque.setJMenuBar(menuBar);
		
		JMenu mnArquivo = new JMenu("Usuario");
		menuBar.add(mnArquivo);
		
		JMenuItem mntmMenu = new JMenuItem("Menu Principal");
		mntmMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmEstoque.dispose();
				InterfaceMenu ir = new InterfaceMenu();
				ir.setVisible(true);
			}
		});
		mnArquivo.add(mntmMenu);
		
		JMenuItem mntmSairLogin = new JMenuItem("Sair");
		mntmSairLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmEstoque.dispose();
				InterfaceLogin ir = new InterfaceLogin();
				ir.setVisible(true);
			}
		});
		mnArquivo.add(mntmSairLogin);
		frmEstoque.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(12, 0, 721, 439);
		frmEstoque.getContentPane().add(panel);
		panel.setLayout(null);
		
		JButton btnAlterar = new JButton("Alterar/Remover Produto");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmEstoque.dispose();
				AlterarEstoque ir = new AlterarEstoque();
				ir.setVisible(true);
			}
		});
		btnAlterar.setBounds(135, 216, 209, 25);
		panel.add(btnAlterar);
		
		JButton btnAdicionarProduto = new JButton("Adicionar Produto");
		btnAdicionarProduto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				frmEstoque.dispose();
				AdicionarEstoque irAdicionar = new AdicionarEstoque();
				irAdicionar.setVisible(true);
			}
		});
		btnAdicionarProduto.setBounds(370, 216, 172, 25);
		panel.add(btnAdicionarProduto);
		
		JLabel label = new JLabel("Tô com Fome, Quero Mais!");
		label.setBounds(513, 412, 185, 15);
		panel.add(label);
	}

	public void setVisible(boolean b) {
		frmEstoque.setVisible(true);
		
	}
}
