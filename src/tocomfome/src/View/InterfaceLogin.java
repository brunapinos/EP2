package View;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.HeadlessException;

import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.border.EtchedBorder;
import javax.swing.JPasswordField;

import Controller.CadastrarFuncionario;
import Model.Funcionario;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;


public class InterfaceLogin {

	private JFrame frmTComFome;
	private JTextField areaUsuario;
	private JPasswordField areaSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceLogin window = new InterfaceLogin();
					window.frmTComFome.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfaceLogin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings("deprecation")
	private void initialize() {
		frmTComFome = new JFrame();
		frmTComFome.setTitle("Tô com Fome");
		frmTComFome.getContentPane().setFont(new Font("Dialog", Font.PLAIN, 36));
		frmTComFome.getContentPane().setLayout(null);
		
		JLabel lblTComFome = new JLabel("Tô com Fome, Quero Mais!" );
		lblTComFome.setFont(new Font("Dialog", Font.BOLD, 20));
		lblTComFome.setBounds(205, 122, 334, 54);
		frmTComFome.getContentPane().add(lblTComFome);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(244, 188, 225, 122);
		frmTComFome.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Login");
		lblNewLabel.setBounds(6, 23, 88, 25);
		panel.add(lblNewLabel);
		
		areaUsuario = new JTextField();
		areaUsuario.setBounds(101, 21, 112, 30);
		panel.add(areaUsuario);
		areaUsuario.setColumns(10);
		
		JLabel label = new JLabel("Senha");
		label.setBounds(6, 63, 112, 30);
		panel.add(label);
		
		areaSenha = new JPasswordField();
		areaSenha.setEchoChar('*');
		areaSenha.setBounds(101, 69, 112, 30);
		panel.add(areaSenha);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				CadastrarFuncionario cadastro = new CadastrarFuncionario();
				
				Funcionario funcionario = new Funcionario();
				
				funcionario.setLogin(areaUsuario.getText());
				funcionario.setSenha(areaSenha.getText());
				
				try {
					if(cadastro.verificarItem(funcionario)) {
						// senha

								frmTComFome.dispose();
								InterfaceMenu irMenu = new InterfaceMenu();
									irMenu.setVisible(true);
					}	
					
						else {// limpa entrada da senha
								

							JOptionPane.showMessageDialog(null,"Login ou Senha errado! Tente novamente.");

										areaSenha.setText("");
										areaUsuario.setText("");

						// limpa entrada da senha
						}
				} catch (HeadlessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		
		});
		btnEntrar.setBounds(205, 322, 117, 25);
		frmTComFome.getContentPane().add(btnEntrar);
		
		//Cadastrar
		
		JButton btnCadastrarUsurio = new JButton("Cadastrar Usuário");
		btnCadastrarUsurio.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				frmTComFome.dispose();
				
				CadastroFuncionario irfuncionario = new CadastroFuncionario();
				
				irfuncionario.setVisible(true);
				
			
			}
		});
		btnCadastrarUsurio.setBounds(375, 322, 164, 25);
		frmTComFome.getContentPane().add(btnCadastrarUsurio);
		frmTComFome.setBounds(100, 100, 700, 500);
		frmTComFome.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void setVisible(boolean b) {
		// TODO Auto-generated method stub
		frmTComFome.setVisible(b);
	}
}
