package View;
import java.awt.*;
import java.awt.event.*;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;



public class InterfaceEntrada implements ActionListener {

	private JFrame mainframe;
	private JLabel marca;
	private JLabel login, senha;
	private JLabel status;
	private JTextField areaLogin;
	private JPasswordField areasenha;
	private JPanel painel;

	
	public InterfaceEntrada(){

		prepararPagina();
		criarPagina();
	}

	private void prepararPagina() {
		
		mainframe = new JFrame("Tô com Fome, quero mais!");
		mainframe.setSize(500, 500);
		mainframe.setLocation(500, 200);
		mainframe.setLayout(new GridLayout(3,1));
		
		mainframe.addWindowListener(new WindowAdapter() {
		
			public void windowClosing(WindowEvent windowsEvent){
				System.exit(0);
			}	
		});
		
		
		marca = new JLabel("Tô com fome, Quero Mais!", JLabel.CENTER);
		marca.setFont(new Font("Arial", Font.BOLD, 24));
		marca.setForeground(new Color(200,10,99));
		
		status = new JLabel("", JLabel.CENTER);
		
		status.setSize(350, 100);
		
		painel = new JPanel();
		painel.setLayout(new GridLayout(2,2,10,20));
		painel.setBorder(BorderFactory.createEtchedBorder());
		
		
		mainframe.add(marca);
		mainframe.add(painel);
		mainframe.add(status);
		
		mainframe.setVisible(true);
		
		
		
		
	}

	private void criarPagina() {

		
		login = new JLabel("Login");
		login.setFont(new Font("Arial", Font.BOLD, 12));
		login.setForeground(new Color(200,10,99));
		
		senha = new JLabel("Senha");
		senha.setFont(new Font("Arial", Font.BOLD, 12));
		senha.setForeground(new Color(200,10,99));
		
		
		areaLogin = new JTextField();
		areaLogin.setEnabled(true);
		areaLogin.setSize(0,40);
		areaLogin.setFont(new Font("Arial",Font.ITALIC,12));

	
		
		areasenha = new JPasswordField();
		areasenha.setEchoChar('*');
		areasenha.addActionListener((ActionListener) this);
		
		painel.add(login);
		painel.add(areaLogin);
		painel.add(senha);
		painel.add(areasenha);


		
	}

	@SuppressWarnings("deprecation")
	public void actionPerformed(ActionEvent evento)
	{
		if(areasenha.getText().equals("teste")) {
	// senha
			status.setText("Senha válida\n");
			status.setForeground(Color.blue);
	}
	else {
			status.setText("Senha inválida\n");
			status.setForeground(Color.red);
			areasenha.setText("");
	// limpa entrada da senha
	}
	
	}
	
	
	
	public static void main(String[] args) {
		
		InterfaceEntrada exemplo = new InterfaceEntrada();
		
		
		
	}
	
}
