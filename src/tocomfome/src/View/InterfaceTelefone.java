package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;

import Controller.CadastrarPedidoTelefone;
import Model.Cliente;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.swing.ButtonGroup;

public class InterfaceTelefone {

	private JFrame frmTelefone;
	private JTextField txtNome;
	private JTextField txtTelefone;
	private JTextField txtEndereco;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txtReserva;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceTelefone window = new InterfaceTelefone();
					window.frmTelefone.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfaceTelefone() {
		JOptionPane.showMessageDialog(null,"Caso seja Reserva de Mesa, NÃO NECESSITA MUDAR ENDEREÇO! ");
		JOptionPane.showMessageDialog(null,"Separe as palavras do ENDERECO por '-' ");
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTelefone = new JFrame();
		frmTelefone.setBounds(100, 100, 705, 500);
		frmTelefone.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTelefone.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 0, 681, 466);
		frmTelefone.getContentPane().add(panel);
		panel.setLayout(null);
		
		JButton btnAvancar = new JButton("Avançar");
		btnAvancar.setBounds(338, 296, 117, 25);
		panel.add(btnAvancar);
		
		JLabel label = new JLabel("Tô com Fome, Quero Mais!");
		label.setBounds(484, 379, 185, 15);
		panel.add(label);
		
		JLabel label_1 = new JLabel("Nome");
		label_1.setBounds(45, 175, 70, 15);
		panel.add(label_1);
		
		txtNome = new JTextField();
		txtNome.setColumns(10);
		txtNome.setBounds(150, 173, 178, 19);
		panel.add(txtNome);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(45, 121, 70, 15);
		panel.add(lblTelefone);
		
		txtTelefone = new JTextField();
		txtTelefone.setColumns(10);
		txtTelefone.setBounds(150, 119, 178, 19);
		panel.add(txtTelefone);
		
		JLabel lblEndereo = new JLabel("Endereço");
		lblEndereo.setBounds(45, 235, 94, 15);
		panel.add(lblEndereo);
		
		txtEndereco = new JTextField();
		txtEndereco.setColumns(10);
		txtEndereco.setBounds(150, 233, 178, 19);
		panel.add(txtEndereco);
		
		JLabel lblReservaDeMesa = new JLabel("Reserva de Mesa");
		lblReservaDeMesa.setBounds(374, 159, 132, 15);
		panel.add(lblReservaDeMesa);
		
		// Pesquisa de Telefone
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				CadastrarPedidoTelefone cadastro = new CadastrarPedidoTelefone();
				
				try {
					if (cadastro.verificarCliente(txtTelefone.getText()) == true){
						
						String[]vetor = cadastro.mostrarInformacao(txtTelefone.getText());
						txtNome.setText(vetor[1]);
						txtEndereco.setVisible(true);
						txtEndereco.setText(vetor[3]);
						
					
						
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnOk.setFont(new Font("Dialog", Font.BOLD, 8));
		btnOk.setBounds(337, 120, 47, 16);
		panel.add(btnOk);
		
		
		
		JLabel lblSimOuNo = new JLabel("Sim ou Não");
		lblSimOuNo.setBounds(403, 175, 88, 15);
		panel.add(lblSimOuNo);
		
		txtReserva = new JTextField();
		txtReserva.setColumns(10);
		txtReserva.setBounds(509, 157, 160, 19);
		panel.add(txtReserva);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 693, 21);
		frmTelefone.getContentPane().add(menuBar);
		
		JMenu menu_1 = new JMenu("Usuario");
		menuBar.add(menu_1);
		
		JMenuItem menuItem_4 = new JMenuItem("Estoque");
		menuItem_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmTelefone.dispose();
				OpcoesEstoque ir = new OpcoesEstoque();
				ir.setVisible(true);
			}
		});
		menu_1.add(menuItem_4);
		
		JMenuItem menuItem_5 = new JMenuItem("Menu Principal");
		menuItem_5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmTelefone.dispose();
				InterfaceMenu ir = new InterfaceMenu();
				ir.setVisible(true);
			}
		});
		menu_1.add(menuItem_5);
		
		JMenuItem menuItem_6 = new JMenuItem("Sair");
		menuItem_6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmTelefone.dispose();
				InterfaceLogin ir = new InterfaceLogin();
				ir.setVisible(true);
			}
		});
		menu_1.add(menuItem_6);
		
		JMenuItem menuItem_3 = new JMenuItem("Cardápio");
		menuItem_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmTelefone.dispose();
				InterfaceCardapio ir = new InterfaceCardapio();
				ir.setVisible(true);
			}
		});
		menuBar.add(menuItem_3);
		btnAvancar.addMouseListener(new MouseAdapter() {
			
			// Botão Avançar
			@Override
			public void mouseReleased(MouseEvent e) {
				
				Cliente cliente = new Cliente();
				CadastrarPedidoTelefone cadastro = new CadastrarPedidoTelefone();
				
				cliente.setNome(txtNome.getText());
				cliente.setTelefone(txtTelefone.getText());
				
				String auxiliar = txtReserva.getText();
				if(auxiliar == "sim"){
					cliente.setNecessidadeEndereco(false);
					cliente.setReservaMesa(true);
					try {
						cadastro.alterarCliente(cliente);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					frmTelefone.dispose();
					
					NovoPedidoTelefone ir = new NovoPedidoTelefone();
					ir.setVisible(true);
				}
				else{
					cliente.setNecessidadeEndereco(true);
					cliente.setReservaMesa(false);
					cliente.setEndereco(txtEndereco.getText());
					
					try {
						cadastro.alterarCliente(cliente);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					NovoPedidoTelefone ir = new NovoPedidoTelefone();
					ir.setVisible(true);
				}
				
			}
		});
	}
	

	public void setVisible(boolean b) {
		// TODO Auto-generated method stub
		frmTelefone.setVisible(b);
	}
}
