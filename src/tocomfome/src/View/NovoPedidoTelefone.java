package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import javax.swing.JList;

public class NovoPedidoTelefone {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NovoPedidoTelefone window = new NovoPedidoTelefone();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NovoPedidoTelefone() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 715, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 12, 715, 467);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblMesa = new JLabel("Mesa");
		lblMesa.setBounds(12, 64, 38, 15);
		panel.add(lblMesa);
		
		JLabel lblPratos = new JLabel("Pratos");
		lblPratos.setBounds(12, 139, 83, 15);
		panel.add(lblPratos);
		
		JLabel lblBebidas = new JLabel("Bebidas");
		lblBebidas.setBounds(12, 219, 62, 15);
		panel.add(lblBebidas);
		
		JLabel lblSobremesa = new JLabel("Sobremesa");
		lblSobremesa.setBounds(12, 302, 83, 15);
		panel.add(lblSobremesa);
		
		JComboBox comboBoxMesa = new JComboBox();
		comboBoxMesa.setModel(new DefaultComboBoxModel(new String[] {""}));
		comboBoxMesa.setMaximumRowCount(30);
		comboBoxMesa.setBounds(113, 59, 155, 24);
		panel.add(comboBoxMesa);
		
		JComboBox comboBoxPratos = new JComboBox();
		comboBoxPratos.setMaximumRowCount(30);
		comboBoxPratos.setBounds(113, 134, 155, 24);
		panel.add(comboBoxPratos);
		
		JComboBox comboBoxBebidas = new JComboBox();
		comboBoxBebidas.setMaximumRowCount(30);
		comboBoxBebidas.setBounds(113, 214, 155, 24);
		panel.add(comboBoxBebidas);
		
		JComboBox comboBoxSobre = new JComboBox();
		comboBoxSobre.setMaximumRowCount(30);
		comboBoxSobre.setBounds(113, 297, 155, 24);
		panel.add(comboBoxSobre);
		
		JButton btnOkPratos = new JButton("OK");
		btnOkPratos.setBounds(394, 134, 54, 25);
		panel.add(btnOkPratos);
		
		JButton btnOkBebidas = new JButton("OK");
		btnOkBebidas.setBounds(394, 214, 54, 25);
		panel.add(btnOkBebidas);
		
		JButton btnOkSobre = new JButton("OK");
		btnOkSobre.setBounds(394, 297, 54, 25);
		panel.add(btnOkSobre);
		
		JLabel label = new JLabel("Qt.");
		label.setBounds(286, 139, 38, 15);
		panel.add(label);
		
		JLabel label_1 = new JLabel("Qt.");
		label_1.setBounds(286, 219, 38, 15);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("Qt.");
		label_2.setBounds(286, 302, 38, 15);
		panel.add(label_2);
		
		JComboBox boxQtPratos = new JComboBox();
		boxQtPratos.setBounds(320, 134, 62, 24);
		panel.add(boxQtPratos);
		
		JComboBox boxQtBebidas = new JComboBox();
		boxQtBebidas.setBounds(320, 214, 62, 24);
		panel.add(boxQtBebidas);
		
		JComboBox boxQtSobre = new JComboBox();
		boxQtSobre.setBounds(320, 297, 62, 24);
		panel.add(boxQtSobre);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_1.setBounds(452, 12, 251, 386);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JButton btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnConfirmar.setEnabled(false);
		btnConfirmar.setBounds(10, 353, 107, 25);
		panel_1.add(btnConfirmar);
		
		JButton btnAvancar = new JButton("Avançar");
		btnAvancar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAvancar.setBounds(129, 353, 107, 25);
		panel_1.add(btnAvancar);
		btnAvancar.setEnabled(false);
		
		JList listPedidos = new JList();
		listPedidos.setBounds(12, 340, 224, -327);
		panel_1.add(listPedidos);
		
		JLabel label_3 = new JLabel("Tô com Fome, Quero Mais!");
		label_3.setBounds(507, 410, 185, 15);
		panel.add(label_3);
		
		JLabel lblPagamento = new JLabel("Pagamento");
		lblPagamento.setBounds(12, 367, 83, 15);
		panel.add(lblPagamento);
		
		JComboBox boxPagamento = new JComboBox();
		boxPagamento.setEnabled(false);
		boxPagamento.setMaximumRowCount(30);
		boxPagamento.setBounds(113, 362, 155, 24);
		panel.add(boxPagamento);
		
		JButton btnOkPagamento = new JButton("OK");
		btnOkPagamento.setEnabled(false);
		btnOkPagamento.setBounds(394, 362, 54, 25);
		panel.add(btnOkPagamento);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnUsurio = new JMenu("Usuário");
		menuBar.add(mnUsurio);
		
		JMenuItem mntmEstoque = new JMenuItem("Estoque");
		mnUsurio.add(mntmEstoque);
		
		JMenuItem mntmPedidos = new JMenuItem("Pedidos");
		mnUsurio.add(mntmPedidos);
		
		JMenuItem mntmMenuPrincipal = new JMenuItem("Menu Principal");
		mnUsurio.add(mntmMenuPrincipal);
		
		JMenuItem mntmSair = new JMenuItem("Sair");
		mnUsurio.add(mntmSair);
		
		JMenu mnArquivo = new JMenu("Arquivo");
		menuBar.add(mnArquivo);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnArquivo.add(mntmCopiar);
		
		JMenuItem mntmRecortar = new JMenuItem("Recortar");
		mnArquivo.add(mntmRecortar);
		
		JMenuItem mntmColar = new JMenuItem("Colar");
		mnArquivo.add(mntmColar);
	}

	public void setVisible(boolean b) {
		// TODO Auto-generated method stub
		frame.setVisible(b);
	}
}
