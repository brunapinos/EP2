package View;

import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.AbstractButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JTree;
import javax.swing.ButtonGroup;

import Controller.CadastrarEstoque;
import Model.Bebida;
import Model.Pratos;
import Model.Sobremesa;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Enumeration;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdicionarEstoque {

	private JFrame frmAdicionarProduto;
	private JTextField txtNome;
	private JTextField txtPreco;
	private JTextField txtQuantidade;
	private JTextField txtVolume;
	private JTextField txtServe;
	private JTextField txtcodigo;
	private JTextField txtSim;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdicionarEstoque window = new AdicionarEstoque();
					window.frmAdicionarProduto.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AdicionarEstoque() {
		JOptionPane.showMessageDialog(null, "Escolha UM tipo de Produto e NÃO ALTERE OS OUTROS");
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAdicionarProduto = new JFrame();
		frmAdicionarProduto.setTitle("Adicionar Produto - Tô com Fome");
		frmAdicionarProduto.setBounds(100, 100, 719, 454);
		frmAdicionarProduto.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAdicionarProduto.getContentPane().setLayout(null);
		
		JLabel label_1 = new JLabel("Tô com Fome, Quero Mais!");
		label_1.setBounds(498, 378, 185, 15);
		frmAdicionarProduto.getContentPane().add(label_1);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(59, 128, 70, 15);
		frmAdicionarProduto.getContentPane().add(lblNome);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(399, 142, 70, 15);
		frmAdicionarProduto.getContentPane().add(lblTipo);
		
		JLabel lblPreco = new JLabel("Preço");
		lblPreco.setBounds(59, 181, 70, 15);
		frmAdicionarProduto.getContentPane().add(lblPreco);
		
		JLabel lblQuantidade = new JLabel("Quantidade");
		lblQuantidade.setBounds(59, 234, 94, 15);
		frmAdicionarProduto.getContentPane().add(lblQuantidade);
	

		txtPreco = new JTextField();
		txtPreco.setColumns(10);
		txtPreco.setBounds(164, 179, 178, 19);
		frmAdicionarProduto.getContentPane().add(txtPreco);
		
		txtQuantidade = new JTextField();
		txtQuantidade.setColumns(10);
		txtQuantidade.setBounds(164, 232, 178, 19);
		frmAdicionarProduto.getContentPane().add(txtQuantidade);
		
		txtNome = new JTextField();
		txtNome.setBounds(164, 126, 178, 19);
		frmAdicionarProduto.getContentPane().add(txtNome);
		txtNome.setColumns(10);
		

		JButton btnAvancar = new JButton("Avançar");
		btnAvancar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				CadastrarEstoque cadastro = new CadastrarEstoque();
			
				if(txtSim.getText().equalsIgnoreCase("sim") && txtVolume.getText().equalsIgnoreCase("nulo") && txtServe.getText().equalsIgnoreCase("nulo")){
					Sobremesa sobremesa = new Sobremesa();
					sobremesa.setCodigo(Integer.valueOf(txtcodigo.getText()).intValue());
					sobremesa.setNome(txtNome.getText());
					sobremesa.setPreco(Double.valueOf(txtPreco.getText()).doubleValue());
					
					try {
						cadastro.adicionarSobremesa(sobremesa);

					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						if(cadastro.verificarSobremesa(sobremesa))
							JOptionPane.showMessageDialog(null, "Cadastro feito com Sucesso!");
					} catch (HeadlessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					frmAdicionarProduto.dispose();
					
					OpcoesEstoque ir = new OpcoesEstoque();
					ir.setVisible(true);
				}
				
				else if(txtSim.getText().equalsIgnoreCase("não") && !txtVolume.getText().equalsIgnoreCase("nulo") && txtServe.getText().equals("nulo")){
					Bebida bebida = new Bebida();
					bebida.setCodigo(Integer.valueOf(txtcodigo.getText()).intValue());
					bebida.setNome(txtNome.getText());
					bebida.setPreco(Double.valueOf(txtPreco.getText()).doubleValue());
				
					try {
						cadastro.adicionarBebida(bebida);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						if(cadastro.verificarBebida(bebida))
						JOptionPane.showMessageDialog(null, "Cadastro feito com Sucesso!");
					} catch (HeadlessException | IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					frmAdicionarProduto.dispose();
					
					OpcoesEstoque ir = new OpcoesEstoque();
					ir.setVisible(true);
				}
				
				else if(txtSim.getText().equalsIgnoreCase("não") && txtVolume.getText().equalsIgnoreCase("nulo") && !txtServe.getText().equals("nulo")){
					Pratos prato = new Pratos();
					prato.setCodigo(Integer.valueOf(txtcodigo.getText()).intValue());
					prato.setNome(txtNome.getText());
					prato.setPreco(Double.valueOf(txtPreco.getText()).doubleValue());
					
					try {
						cadastro.adicionarPrato(prato);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						if(cadastro.verificarPrato(prato))
							JOptionPane.showMessageDialog(null, "Cadastro feito com Sucesso!");
					} catch (HeadlessException | IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					frmAdicionarProduto.dispose();
					
					OpcoesEstoque ir = new OpcoesEstoque();
					ir.setVisible(true);
				}
				
				else{
					JOptionPane.showMessageDialog(null, "Entradas INVALIDAS! Digite Novemente");
				}
				
			}
		});
		btnAvancar.setBounds(352, 295, 117, 25);
		
		frmAdicionarProduto.getContentPane().add(btnAvancar);
		
		JLabel lblBebida = new JLabel("Bebida");
		lblBebida.setBounds(520, 104, 70, 15);
		frmAdicionarProduto.getContentPane().add(lblBebida);
		
		JLabel lblVolumeDaEmbalagem = new JLabel("Volume da Embalagem");
		lblVolumeDaEmbalagem.setBounds(469, 132, 170, 15);
		frmAdicionarProduto.getContentPane().add(lblVolumeDaEmbalagem);
		
		txtVolume = new JTextField();
		txtVolume.setText("nulo");
		txtVolume.setColumns(10);
		txtVolume.setBounds(469, 159, 178, 19);
		frmAdicionarProduto.getContentPane().add(txtVolume);
		
		txtServe = new JTextField();
		txtServe.setText("nulo");
		txtServe.setColumns(10);
		txtServe.setBounds(469, 246, 178, 19);
		frmAdicionarProduto.getContentPane().add(txtServe);
		
		JLabel lblNDePessoas = new JLabel("Nº de Pessoas que Serve");
		lblNDePessoas.setBounds(469, 219, 192, 15);
		frmAdicionarProduto.getContentPane().add(lblNDePessoas);
		
		JLabel lblPratos = new JLabel("Pratos");
		lblPratos.setBounds(520, 191, 70, 15);
		frmAdicionarProduto.getContentPane().add(lblPratos);
		
		JLabel lblCdigo = new JLabel("Código");
		lblCdigo.setBounds(59, 78, 70, 15);
		frmAdicionarProduto.getContentPane().add(lblCdigo);
		
		txtcodigo = new JTextField();
		txtcodigo.setColumns(10);
		txtcodigo.setBounds(164, 76, 178, 19);
		frmAdicionarProduto.getContentPane().add(txtcodigo);
		
		JLabel lblSobremesa = new JLabel("É Sobremesa ");
		lblSobremesa.setBounds(500, 40, 103, 15);
		frmAdicionarProduto.getContentPane().add(lblSobremesa);
		
		txtSim = new JTextField();
		txtSim.setText("sim");
		txtSim.setColumns(10);
		txtSim.setBounds(469, 67, 178, 19);
		frmAdicionarProduto.getContentPane().add(txtSim);
		
		JMenuBar menuBar = new JMenuBar();
		frmAdicionarProduto.setJMenuBar(menuBar);
		
		JMenu menu = new JMenu("Usuario");
		menuBar.add(menu);
		
		JMenuItem menuItem = new JMenuItem("Estoque");
		menuItem.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmAdicionarProduto.dispose();
				OpcoesEstoque ir = new OpcoesEstoque();
				ir.setVisible(true);
			}
		});
		menu.add(menuItem);
		
		JMenuItem menuItem_1 = new JMenuItem("Menu Principal");
		menuItem_1.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(MouseEvent e) {
				frmAdicionarProduto.dispose();
				InterfaceMenu ir = new InterfaceMenu();
				ir.setVisible(true);
			}
		});
		menu.add(menuItem_1);
		
		JMenuItem menuItem_2 = new JMenuItem("Sair");
		menuItem_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmAdicionarProduto.dispose();
				InterfaceLogin ir = new InterfaceLogin();
				ir.setVisible(true);
			}
		});
		menu.add(menuItem_2);
		
		JMenuItem menuItem_6 = new JMenuItem("Cardápio");
		menuItem_6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmAdicionarProduto.dispose();
				InterfaceCardapio ir = new InterfaceCardapio();
				ir.setVisible(true);
			}
		});
		menuBar.add(menuItem_6);
		
		
	
	}
		
	

	public void setVisible(boolean b) {
		// TODO Auto-generated method stub
		frmAdicionarProduto.setVisible(true);
	}
}
