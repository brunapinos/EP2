package View;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenu;
import javax.swing.SwingConstants;

public class InterfaceMenu {

	private JFrame frmMenuPrincipal;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceMenu window = new InterfaceMenu();
					window.frmMenuPrincipal.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfaceMenu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMenuPrincipal = new JFrame();
		frmMenuPrincipal.setTitle("Menu Principal - Tô com Fome");
		frmMenuPrincipal.setBounds(100, 100, 700, 500);
		frmMenuPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMenuPrincipal.getContentPane().setLayout(null);
		
		
		// Botão Estoque
		JButton button = new JButton("Estoque");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				frmMenuPrincipal.dispose();
				OpcoesEstoque irEstoque = new OpcoesEstoque();
				irEstoque.setVisible(true);
			}
		});
		button.setBounds(182, 196, 117, 25);
		frmMenuPrincipal.getContentPane().add(button);
		
		// Botão Pedidos
		JButton btnPedidos = new JButton("Pedidos");
		btnPedidos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			
				frmMenuPrincipal.dispose();
				InterfacePedidos irPedidos = new InterfacePedidos();
				irPedidos.setVisible(true);
			}
		});
		btnPedidos.setBounds(406, 196, 117, 25);
		frmMenuPrincipal.getContentPane().add(btnPedidos);
		
		JLabel lblTComFome = new JLabel("Tô com Fome, Quero Mais!");
		lblTComFome.setBounds(503, 413, 185, 15);
		frmMenuPrincipal.getContentPane().add(lblTComFome);
		
		JMenuBar menuBar = new JMenuBar();
		frmMenuPrincipal.setJMenuBar(menuBar);
		
		JMenu mnUsuario = new JMenu("Usuario");
		menuBar.add(mnUsuario);
		
		JMenuItem mntmLogin = new JMenuItem("Sair");
		mntmLogin.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(MouseEvent e) {
				frmMenuPrincipal.dispose();
				InterfaceLogin ir = new InterfaceLogin();
				ir.setVisible(true);
			}
		});
		mnUsuario.add(mntmLogin);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
	
	public void setVisible(boolean b){
		frmMenuPrincipal.setVisible(b);
	}
}
