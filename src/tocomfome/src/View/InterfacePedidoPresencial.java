package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class InterfacePedidoPresencial {

	private JFrame frmPedidoP;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfacePedidoPresencial window = new InterfacePedidoPresencial();
					window.frmPedidoP.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfacePedidoPresencial() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPedidoP = new JFrame();
		frmPedidoP.setBounds(100, 100, 700, 500);
		frmPedidoP.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frmPedidoP.setJMenuBar(menuBar);
		
		JMenu mnUsurio = new JMenu("Usuário");
		menuBar.add(mnUsurio);
		
		JMenuItem mntmEstoque = new JMenuItem("Estoque");
		mntmEstoque.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(MouseEvent e) {
				frmPedidoP.dispose();
				OpcoesEstoque ir = new OpcoesEstoque();
				ir.setVisible(true);
			}
		});
		mnUsurio.add(mntmEstoque);
		
		JMenuItem mntmPedido = new JMenuItem("Pedido");
		mntmPedido.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmPedidoP.dispose();
				InterfacePedidos ir = new InterfacePedidos();
				ir.setVisible(true);
			}
		});
		mnUsurio.add(mntmPedido);
		
		JMenuItem mntmMenuPrincipal = new JMenuItem("Menu Principal");
		mntmMenuPrincipal.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmPedidoP.dispose();
				InterfaceMenu ir = new InterfaceMenu();
				ir.setVisible(true);
			}
		});
		mnUsurio.add(mntmMenuPrincipal);
		
		JMenuItem mntmSair = new JMenuItem("Sair");
		mntmSair.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmPedidoP.dispose();
				InterfaceLogin ir = new InterfaceLogin();
				ir.setVisible(true);
			}
		});
		mnUsurio.add(mntmSair);
		
		JMenuItem mntmCardpio = new JMenuItem("Cardápio");
		mntmCardpio.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmPedidoP.dispose();
				InterfaceCaradapio ir = new InterfaceCardapio();
				ir.setVisible(true);
			}
		});
		menuBar.add(mntmCardpio);
		frmPedidoP.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(12, 0, 688, 439);
		frmPedidoP.getContentPane().add(panel);
		panel.setLayout(null);
		
		JButton btnAdicionarMesa = new JButton("Adicionar Pedido");
		btnAdicionarMesa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmPedidoP.dispose();
				NovoPedidoPresencial ir = new NovoPedidoPresencial();
				ir.setVisible(true);
			}
		});
		btnAdicionarMesa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAdicionarMesa.setBounds(115, 190, 172, 25);
		panel.add(btnAdicionarMesa);
		
		JLabel label = new JLabel("Tô com Fome, Quero Mais!");
		label.setBounds(472, 412, 185, 15);
		panel.add(label);
		
		JButton btnPagamento = new JButton("Pagamento");
		btnPagamento.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmPedidoP.dispose();
				InterfacePagamento ir = new InterfacePagamento();
				ir.setVisible(true);
			}
		});
		btnPagamento.setBounds(385, 190, 117, 25);
		panel.add(btnPagamento);
	}

	public void setVisible(boolean b) {
		// TODO Auto-generated method stub
		frmPedidoP.setVisible(b);
	}

}
