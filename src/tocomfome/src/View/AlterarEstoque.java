package View;

import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.AbstractButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JTree;
import javax.swing.ButtonGroup;

import Controller.CadastrarEstoque;
import Model.Bebida;
import Model.Pratos;
import Model.Sobremesa;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Enumeration;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AlterarEstoque {

	private JFrame frmA;
	private JTextField txtNome;
	private JTextField txtPreco;
	private JTextField txtQuantidade;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txtCodigo;
	private JTextField txtSim;
	private JTextField txtVolume;
	private JTextField txtServe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AlterarEstoque window = new AlterarEstoque();
					window.frmA.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AlterarEstoque() {
		JOptionPane.showMessageDialog(null, "Escolha SOMENTE UM tipo de PRODUTO e não ALTERE OS OUTROS CAMPOS!");
		JOptionPane.showMessageDialog(null, "Preencha TODOS os campos do PRODUTO ESCOLHIDO! ");
		JOptionPane.showMessageDialog(null, "O CóDIGO DO PRODUTO É INALTERÁVEL!");
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmA = new JFrame();
		frmA.setTitle("Alterar/Remover Produto - Tô com Fome");
		frmA.setBounds(100, 100, 719, 454);
		frmA.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmA.getContentPane().setLayout(null);
		
		JLabel label_1 = new JLabel("Tô com Fome, Quero Mais!");
		label_1.setBounds(498, 378, 185, 15);
		frmA.getContentPane().add(label_1);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(59, 128, 70, 15);
		frmA.getContentPane().add(lblNome);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(399, 142, 70, 15);
		frmA.getContentPane().add(lblTipo);
		
		JLabel lblPreco = new JLabel("Preço");
		lblPreco.setBounds(59, 181, 70, 15);
		frmA.getContentPane().add(lblPreco);
		
		JLabel lblQuantidade = new JLabel("Quantidade");
		lblQuantidade.setBounds(59, 234, 94, 15);
		frmA.getContentPane().add(lblQuantidade);
	
		
		txtPreco = new JTextField();
		txtPreco.setColumns(10);
		txtPreco.setBounds(164, 179, 178, 19);
		frmA.getContentPane().add(txtPreco);
		
		txtQuantidade = new JTextField();
		txtQuantidade.setColumns(10);
		txtQuantidade.setBounds(164, 232, 178, 19);
		frmA.getContentPane().add(txtQuantidade);
		
		txtNome = new JTextField();
		txtNome.setBounds(164, 126, 178, 19);
		frmA.getContentPane().add(txtNome);
		txtNome.setColumns(10);
		

		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.setBounds(283, 295, 117, 25);
		
		frmA.getContentPane().add(btnAlterar);
		
		JLabel lblCdigo = new JLabel("Código");
		lblCdigo.setBounds(59, 76, 70, 15);
		frmA.getContentPane().add(lblCdigo);
		
		txtCodigo = new JTextField();
		txtCodigo.setColumns(10);
		txtCodigo.setBounds(164, 74, 178, 19);
		frmA.getContentPane().add(txtCodigo);
		
		JButton btnRemover = new JButton("Remover");
		btnRemover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			
				
				CadastrarEstoque cadastro = new CadastrarEstoque();
			
				if(txtSim.getText().equalsIgnoreCase("sim") && txtVolume.getText().equalsIgnoreCase("nulo") && txtServe.getText().equalsIgnoreCase("nulo")){
					Sobremesa sobremesa = new Sobremesa();
					sobremesa.setCodigo(Integer.valueOf(txtCodigo.getText()).intValue());
					sobremesa.setNome(txtNome.getText());
					sobremesa.setPreco(Double.valueOf(txtPreco.getText()).doubleValue());
					
					try {
						cadastro.remocaoSobremesa(sobremesa);

					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						if(cadastro.verificarSobremesa(sobremesa))
							JOptionPane.showMessageDialog(null, "Remoção feita com Sucesso!");
					} catch (HeadlessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					frmA.dispose();
					
					OpcoesEstoque ir = new OpcoesEstoque();
					ir.setVisible(true);
				}
				
				else if(txtSim.getText().equalsIgnoreCase("não") && !txtVolume.getText().equalsIgnoreCase("nulo") && txtServe.getText().equals("nulo")){
					Bebida bebida = new Bebida();
					bebida.setCodigo(Integer.valueOf(txtCodigo.getText()).intValue());
					bebida.setNome(txtNome.getText());
					bebida.setPreco(Double.valueOf(txtPreco.getText()).doubleValue());
				
					try {
						cadastro.remocaoBebida(bebida);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						if(cadastro.verificarBebida(bebida))
						JOptionPane.showMessageDialog(null, "Remoção feita com Sucesso!");
					} catch (HeadlessException | IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					frmA.dispose();
					
					OpcoesEstoque ir = new OpcoesEstoque();
					ir.setVisible(true);
				}
				
				else if(txtSim.getText().equalsIgnoreCase("não") && txtVolume.getText().equalsIgnoreCase("nulo") && !txtServe.getText().equals("nulo")){
					Pratos prato = new Pratos();
					prato.setCodigo(Integer.valueOf(txtCodigo.getText()).intValue());
					prato.setNome(txtNome.getText());
					prato.setPreco(Double.valueOf(txtPreco.getText()).doubleValue());
					
					try {
						cadastro.remocaoPrato(prato);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						if(cadastro.verificarPrato(prato))
							JOptionPane.showMessageDialog(null, "Remoção feita com Sucesso!");
					} catch (HeadlessException | IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					frmA.dispose();
					
					OpcoesEstoque ir = new OpcoesEstoque();
					ir.setVisible(true);
				}
				
				else{
					JOptionPane.showMessageDialog(null, "Entradas INVALIDAS! Digite Novemente");
				}
			}	

		});
		btnRemover.setBounds(429, 295, 117, 25);
		frmA.getContentPane().add(btnRemover);
		
		JLabel label = new JLabel("É Sobremesa ");
		label.setBounds(522, 58, 103, 15);
		frmA.getContentPane().add(label);
		
		txtSim = new JTextField();
		txtSim.setText("sim");
		txtSim.setColumns(10);
		txtSim.setBounds(491, 85, 178, 19);
		frmA.getContentPane().add(txtSim);
		
		JLabel label_2 = new JLabel("Bebida");
		label_2.setBounds(542, 122, 70, 15);
		frmA.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("Volume da Embalagem");
		label_3.setBounds(491, 150, 170, 15);
		frmA.getContentPane().add(label_3);
		
		txtVolume = new JTextField();
		txtVolume.setText("nulo");
		txtVolume.setColumns(10);
		txtVolume.setBounds(491, 177, 178, 19);
		frmA.getContentPane().add(txtVolume);
		
		JLabel label_4 = new JLabel("Pratos");
		label_4.setBounds(542, 209, 70, 15);
		frmA.getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel("Nº de Pessoas que Serve");
		label_5.setBounds(491, 237, 192, 15);
		frmA.getContentPane().add(label_5);
		
		txtServe = new JTextField();
		txtServe.setText("nulo");
		txtServe.setColumns(10);
		txtServe.setBounds(491, 264, 178, 19);
		frmA.getContentPane().add(txtServe);
		
		JMenuBar menuBar = new JMenuBar();
		frmA.setJMenuBar(menuBar);
		
		JMenu menu = new JMenu("Usuario");
		menuBar.add(menu);
		
		JMenuItem menuItem = new JMenuItem("Estoque");
		menuItem.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmA.dispose();
				OpcoesEstoque ir = new OpcoesEstoque();
				ir.setVisible(true);
			}
		});
		menu.add(menuItem);
		
		JMenuItem menuItem_1 = new JMenuItem("Menu Principal");
		menuItem_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmA.dispose();
				InterfaceMenu ir = new InterfaceMenu();
				ir.setVisible(true);
			}
		});
		menu.add(menuItem_1);
		
		JMenuItem menuItem_2 = new JMenuItem("Sair");
		menuItem_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmA.dispose();
				InterfaceLogin ir = new InterfaceLogin();
				ir.setVisible(true);
			}
		});
		menu.add(menuItem_2);
		
		JMenuItem menuItem_6 = new JMenuItem("Cardápio");
		menuItem_6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				frmA.dispose();
				InterfaceCardapio ir = new InterfaceCardapio();
				ir.setVisible(true);
			}
		});
		menuBar.add(menuItem_6);
		
		// Ação do Botão Avançar
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				
				CadastrarEstoque cadastro = new CadastrarEstoque();
			
				if(txtSim.getText().equalsIgnoreCase("sim") && txtVolume.getText().equalsIgnoreCase("nulo") && txtServe.getText().equalsIgnoreCase("nulo")){
					Sobremesa sobremesa = new Sobremesa();
					sobremesa.setCodigo(Integer.valueOf(txtCodigo.getText()).intValue());
					sobremesa.setNome(txtNome.getText());
					sobremesa.setPreco(Double.valueOf(txtPreco.getText()).doubleValue());
					
					try {
						cadastro.alteracaoSobremesa(sobremesa);

					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						if(cadastro.verificarSobremesa(sobremesa))
							JOptionPane.showMessageDialog(null, "Alteração feita com Sucesso!");
					} catch (HeadlessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					frmA.dispose();
					
					OpcoesEstoque ir = new OpcoesEstoque();
					ir.setVisible(true);
				}
				
				else if(txtSim.getText().equalsIgnoreCase("não") && !txtVolume.getText().equalsIgnoreCase("nulo") && txtServe.getText().equals("nulo")){
					Bebida bebida = new Bebida();
					bebida.setCodigo(Integer.valueOf(txtCodigo.getText()).intValue());
					bebida.setNome(txtNome.getText());
					bebida.setPreco(Double.valueOf(txtPreco.getText()).doubleValue());
				
					try {
						cadastro.alteracaoBebida(bebida);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						if(cadastro.verificarBebida(bebida))
						JOptionPane.showMessageDialog(null, "Alteração feita com Sucesso!");
					} catch (HeadlessException | IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					frmA.dispose();
					
					OpcoesEstoque ir = new OpcoesEstoque();
					ir.setVisible(true);
				}
				
				else if(txtSim.getText().equalsIgnoreCase("não") && txtVolume.getText().equalsIgnoreCase("nulo") && !txtServe.getText().equals("nulo")){
					Pratos prato = new Pratos();
					prato.setCodigo(Integer.valueOf(txtCodigo.getText()).intValue());
					prato.setNome(txtNome.getText());
					prato.setPreco(Double.valueOf(txtPreco.getText()).doubleValue());
					
					try {
						cadastro.alteracaoPrato(prato);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						if(cadastro.verificarPrato(prato))
							JOptionPane.showMessageDialog(null, "Alteração feita com Sucesso!");
					} catch (HeadlessException | IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					frmA.dispose();
					
					OpcoesEstoque ir = new OpcoesEstoque();
					ir.setVisible(true);
				}
				
				else{
					JOptionPane.showMessageDialog(null, "Entradas INVALIDAS! Digite Novemente");
				}
			}	
		});
	 }
	


	public void setVisible(boolean b) {
		// TODO Auto-generated method stub
		frmA.setVisible(true);
	}
}
