package Controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import Model.Cliente;
import Model.PedidoTelefone;
import Model.Produtos;

public class CadastrarPedidoTelefone {

	// Métodos com Relação ao Cliente
	public boolean verificarCliente(String telefone) throws IOException{
		

		File path = new File("./doc/clientes.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
	
		

		String auxiliar = lerArquivo.readLine();

		
		while(auxiliar != null){
			
			if(!auxiliar.startsWith(telefone)){
				auxiliar = lerArquivo.readLine();

		      }
			else{
				lerArquivo.close();
				return true;
			}


		}
			
		lerArquivo.close();
		return false;

		
	}


	public void alterarCliente(Cliente cliente) throws IOException{
		
		

		File path = new File("./doc/clientes.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
		String verifica = cliente.getTelefone() + " " + cliente.getNome() + " " + cliente.isNecessidadeEndereco()+
				 " " +cliente.getEndereco()+ " " + cliente.isReservaMesa() + "\n";
		
		

		
		File copia = new File("./doc/clientescopia.txt");
		
		BufferedWriter arquivo;
		arquivo = new BufferedWriter(new FileWriter(copia, true));

		String auxiliar = lerArquivo.readLine();

		
		while(auxiliar != null){
			
			if(!auxiliar.startsWith(cliente.getTelefone())){
				
				String gravacao = auxiliar;
				arquivo.append(gravacao);
				auxiliar = lerArquivo.readLine();

		      }
			else{
				arquivo.append(verifica);
				auxiliar = lerArquivo.readLine();
			}


		}
		
		if(verificarCliente(cliente.getTelefone()) == false){
		
			arquivo.append(verifica);
		}
		
		arquivo.close();	
		lerArquivo.close();
	
		path.delete();
		copia.renameTo(path);

}

	public String[] mostrarInformacao(String telefone) throws IOException{
		
		File path = new File("./doc/clientes.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
		
		String auxiliar = lerArquivo.readLine();
		
		
		while(auxiliar != null){
			
			if(!auxiliar.startsWith(telefone)){
				auxiliar = lerArquivo.readLine();

		      }
			else{	
				break;
			}
		}
		
		String verifica  = new String();
		String vetor[] = new String [10];
		int i = 0,contador = 0;
		char charaux;
		
	for(i=0; i < auxiliar.length(); i++ ){
		
		charaux = auxiliar.charAt(i);
		
		if(charaux == ' ' || charaux == '\n' ){
			
			vetor[contador] = verifica;
			verifica  = new String();
			contador += 1;
		}
		else{
			verifica = verifica + charaux;
		}
	}	
		
		lerArquivo.close();
		return vetor;
	}

	//Métodos com Relação ao Pedido

	public void salvarPedido(PedidoTelefone pedido) throws IOException{
	
		
		String path = new String("./doc/telefone.txt");
		
		BufferedWriter arquivo;
		arquivo = new BufferedWriter(new FileWriter(path, true));

		pedido.setPrecoTotal(parcelaPrecoTotal(pedido));
	
		String gravacao = pedido.getCliente() + " "+ pedido.getPrecoTotal() + " " + pedido.getProdutos() + " " + pedido.getQuantidade()+
				 " " + pedido.getObservacao()+ " " + pedido.isPago() +  " " + pedido.getTipoPagamento() +"\n";
		
		arquivo.append(gravacao); 		
		arquivo.close();
		
		
	}
	
	public double parcelaPrecoTotal(PedidoTelefone pedido){
		
		Produtos []produto = pedido.getProdutos();
		int []quantidade = pedido.getQuantidade();
		
		double preco = 0;
		
		for(int i=0; i<(produto.length); i++ ){
			
			preco += produto[i].getPreco()*quantidade[i]; 
			
		}
		
		return preco;
	}
	
	public boolean verificarPedido(PedidoTelefone pedido) throws IOException{
		File path = new File("./doc/telefone.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
	
		
		String verifica = pedido.getCliente() + " "+ pedido.getPrecoTotal() + " " + pedido.getProdutos() + " " + pedido.getQuantidade()+
				 " " + pedido.getObservacao()+ " " + pedido.isPago() +  " " + pedido.getTipoPagamento() +"\n";
		String auxiliar = lerArquivo.readLine();

		
		while(auxiliar != null){
			
			if(!auxiliar.equals(verifica)){
				auxiliar = lerArquivo.readLine();

		      }
			else{
				lerArquivo.close();
				return true;
			}


		}
			
		lerArquivo.close();
		return false;

		
	}
}