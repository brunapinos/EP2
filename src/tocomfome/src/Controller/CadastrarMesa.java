package Controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import Model.Pedido;
import Model.PedidoPresencial;
import Model.Produtos;

public class CadastrarMesa{
	
	public void salvarPedido(PedidoPresencial pedido) throws IOException{
	
		
		String path = new String("./doc/" + "presencial.txt");
		
		BufferedWriter arquivo;
		arquivo = new BufferedWriter(new FileWriter(path, true));

		pedido.setPrecoTotal(parcelaPrecoTotal(pedido));
	
		String gravacao = pedido.getMesa() + " "+ pedido.getPrecoTotal() + " " + pedido.getProdutos() + " " + pedido.getQuantidade()+
				 " " + pedido.getObservacao()+ " " + pedido.isPago() +  " " + pedido.getTipoPagamento() +"\n";
		
		arquivo.append(gravacao); 		
		arquivo.close();
		
		
	}
	
	public PedidoPresencial removerPedido(PedidoPresencial pedido) throws IOException{

		int []quantidade = pedido.getQuantidade();
		for(int i = 0; i < quantidade.length; i++){
			
			quantidade[i] = -1 * quantidade[i];
		}
		
		pedido.setQuantidade(quantidade);
		pedido.setPrecoTotal(parcelaPrecoTotal(pedido));
		
		return pedido;
		
	}

	public double parcelaPrecoTotal(PedidoPresencial pedido){
		
		Produtos []produto = pedido.getProdutos();
		int []quantidade = pedido.getQuantidade();
		
		double preco = 0;
		
		for(int i=0; i<(produto.length); i++ ){
			
			preco += produto[i].getPreco()*quantidade[i]; 
			
		}
		
		return preco;
	}
	
	public boolean verificaPedido(PedidoPresencial pedido) throws IOException{
		

		/*DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
		Date date = new Date(0);
		
		String data = dateFormat.format(date);
		*/
		String path = new String("./doc/" + "presencial.txt");
		
		BufferedReader arquivo;
		arquivo = new BufferedReader(new FileReader(path));

		String verifica = pedido.getMesa() + " "+ pedido.getPrecoTotal() + " " + pedido.getProdutos() + " " + pedido.getQuantidade()+
				 " " + pedido.getObservacao()+ " " + pedido.isPago() +  " " + pedido.getTipoPagamento() +"\n";
		
		String auxiliar = arquivo.readLine();
		while(auxiliar != null){
			
			if(auxiliar.equals(verifica)){
				arquivo.close();
				return true;
			}
			
			auxiliar = arquivo.readLine();
		}
		
		arquivo.close();
		return false;
		
	}
}
