package Controller;

import java.io.*;


import Model.Funcionario;

public class CadastrarFuncionario extends Funcionario {

	public void criacaoArquivo(Funcionario funcionario) throws IOException{

		String path = new String("./doc/funcionario.txt");
		
		BufferedWriter arquivo;
		arquivo = new BufferedWriter(new FileWriter(path, true));

	
		String gravacao = funcionario.getLogin() + " " + funcionario.getSenha() + "\n";
		arquivo.append(gravacao); 		
		arquivo.close();

	}



	public void remocaoItem(Funcionario funcionario) throws IOException {
		
		// Abertura do arquivo principal
		File path = new File("./doc/funcionario.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
		String verifica = funcionario.getLogin() + " " + funcionario.getSenha();
		
		
		// Abertura do arquivo auxiliar
		
		File copia = new File("./doc/funcionariocopia.txt");
		
		BufferedWriter arquivo;
		arquivo = new BufferedWriter(new FileWriter(copia, true));

		
		String auxiliar = lerArquivo.readLine();
		
		while(auxiliar != null){
			
			if(verifica != auxiliar){
				
				String gravacao = auxiliar;
				arquivo.append(gravacao);

		      }
			auxiliar = lerArquivo.readLine();

		}
		
		arquivo.close();	
		lerArquivo.close();
	
		path.delete();
		copia.renameTo(path);
		
		
	}


	public boolean verificarItem(Funcionario funcionario) throws IOException {
		
		File path = new File("./doc/funcionario.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
		String verifica = funcionario.getLogin() + " " + funcionario.getSenha();
		
		String auxiliar = lerArquivo.readLine();
		
		while(auxiliar != null){
			
			if(!verifica.equals(auxiliar)){
				
				auxiliar = lerArquivo.readLine();
		      }
			else
				{
					lerArquivo.close();
					return true;
					
				}

		}
		
		return false;

	}
	

}
