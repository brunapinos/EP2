package Controller;

public class Validacao {

	public boolean validaInteiro(String string){
		try {
			
			@SuppressWarnings("unused")
			int numero = Integer.valueOf(string).intValue();
		}
		catch(NumberFormatException ex){
			return false;
		}
		
		return true;

	}
	
	public boolean validaDouble(String string){
		try {
			
			@SuppressWarnings("unused")
			double numero = Double.valueOf(string).intValue();
	
		}
		
		catch(NumberFormatException ex){
			return false;
		}
		
		return true;

	}
	
	public boolean validaString(String string){
	
		if(string == null || string == "" || string == " " || string == "	")
			return false;
		else
		return true;

	}
}
