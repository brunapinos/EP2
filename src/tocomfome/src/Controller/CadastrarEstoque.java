package Controller;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.sun.xml.internal.txw2.Document;

import Model.Bebida;
import Model.Funcionario;
import Model.Pratos;
import Model.Sobremesa;

public class CadastrarEstoque {

	// Criar Produtos no Estoque
	
	public  void adicionarBebida(Bebida bebida) throws IOException {
		

		String path = new String("./doc/bebidas.txt");
		
		BufferedWriter arquivo;
		arquivo = new BufferedWriter(new FileWriter(path, true));

	
		String gravacao = bebida.getCodigo() + ";" + bebida.getNome() + ";" + bebida.getQtEstoque()+
				 ";" +bebida.getPreco()+ ";" + bebida.getVolume() + "\n";
		
		arquivo.append(gravacao); 		
		arquivo.close();
		
	
	}
	
	
	public  void adicionarPrato(Pratos prato) throws IOException {

		String path = new String("./doc/pratos.txt");
		
		BufferedWriter arquivo;
		arquivo = new BufferedWriter(new FileWriter(path, true));

	
		String gravacao = prato.getCodigo() + ";" + prato.getNome() + ";" + prato.getQtEstoque()+
				 ";" +prato.getPreco()+ ";" + prato.getNumServe() + "\n";
		
		arquivo.append(gravacao); 		
		arquivo.close();
		
	}
	
	
	public  void adicionarSobremesa(Sobremesa sobremesa) throws IOException {
		
		String path = new String("./doc/sobremesas.txt");
		
		BufferedWriter arquivo;
		arquivo = new BufferedWriter(new FileWriter(path, true));

	
		String gravacao = sobremesa.getCodigo() + ";" + sobremesa.getNome() + ";" + sobremesa.getQtEstoque()+
				 ";" +sobremesa.getPreco()+ "\n";
		
		arquivo.append(gravacao); 		
		arquivo.close();
		
	}
	
	
	// Alterar Produtos no Estoque
	
	@SuppressWarnings("static-access")
	public  void alteracaoBebida(Bebida bebida) throws IOException {
		
		

				File path = new File("./doc/bebidas.txt");
				BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
				String verifica = bebida.getCodigo() + ";" + bebida.getNome() + ";" + bebida.getQtEstoque()+
						 ";" +bebida.getPreco()+ ";" + bebida.getVolume() + "\n";
				
				

				
				File copia = new File("./doc/bebidascopia.txt");
				
				BufferedWriter arquivo;
				arquivo = new BufferedWriter(new FileWriter(copia, true));
	
				String auxiliar = lerArquivo.readLine();
				String bebidaaux = new String();
				bebidaaux.valueOf(bebida.getCodigo());
				
				while(auxiliar != null){
					
					if(!auxiliar.startsWith(bebidaaux)){
						
						String gravacao = auxiliar;
						arquivo.append(gravacao);
						auxiliar = lerArquivo.readLine();

				      }
					else{
						arquivo.append(verifica);
						auxiliar = lerArquivo.readLine();
					}


				}
				
				arquivo.close();	
				lerArquivo.close();
			
				path.delete();
				copia.renameTo(path);

	}
	
	
	public  void alteracaoPrato(Pratos prato) throws IOException {
		
		
		

				File path = new File("./doc/pratos.txt");
				BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
				String verifica = prato.getCodigo() + ";" + prato.getNome() + ";" + prato.getQtEstoque()+
						 ";" +prato.getPreco()+ ";" + prato.getNumServe() + "\n";
				
				

				
				File copia = new File("./doc/pratoscopia.txt");
				
				BufferedWriter arquivo;
				arquivo = new BufferedWriter(new FileWriter(copia, true));
	
				String auxiliar = lerArquivo.readLine();
				String pratoaux = new String();
				pratoaux.valueOf(prato.getCodigo());
				
				while(auxiliar != null){
					
					if(!auxiliar.startsWith(pratoaux)){
						
						String gravacao = auxiliar;
						arquivo.append(gravacao);
						auxiliar = lerArquivo.readLine();

				      }
					else{
						arquivo.append(verifica);
						auxiliar = lerArquivo.readLine();
					}


				}
				
				arquivo.close();	
				lerArquivo.close();
			
				path.delete();
				copia.renameTo(path);
		
	}
	
	
	public  void alteracaoSobremesa(Sobremesa sobremesa) throws IOException {
		

				File path = new File("./doc/sobremesas.txt");
				BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
				String verifica = sobremesa.getCodigo() + ";" + sobremesa.getNome() + ";" + sobremesa.getQtEstoque()+
						 ";" +sobremesa.getPreco()+ "\n";
				

				
				File copia = new File("./doc/sobremesascopia.txt");
				
				BufferedWriter arquivo;
				arquivo = new BufferedWriter(new FileWriter(copia, true));
	
				String auxiliar = lerArquivo.readLine();
				String sobreaux = new String();
				sobreaux.valueOf(sobremesa.getCodigo());
				
				while(auxiliar != null){
					
					if(!auxiliar.startsWith(sobreaux)){
						
						String gravacao = auxiliar;
						arquivo.append(gravacao);
						auxiliar = lerArquivo.readLine();

				      }
					else{
						arquivo.append(verifica);
						auxiliar = lerArquivo.readLine();
					}


				}
				
				arquivo.close();	
				lerArquivo.close();
			
				path.delete();
				copia.renameTo(path);
	}
	
	// Remover Produtos no Estoque
	
	public  void remocaoBebida(Bebida bebida) throws IOException{

		

				File path = new File("./doc/bebidas.txt");
				BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
				String verifica = bebida.getCodigo() + ";" + bebida.getNome() + ";" + bebida.getQtEstoque()+
						 ";" +bebida.getPreco()+ ";" + bebida.getVolume() + "\n";
				
				
				
				File copia = new File("./doc/bebidascopia.txt");
				
				BufferedWriter arquivo;
				arquivo = new BufferedWriter(new FileWriter(copia, true));
	
				String auxiliar = lerArquivo.readLine();
				
				while(auxiliar != null){
					
					if(verifica != auxiliar){
						
						String gravacao = auxiliar;
						arquivo.append(gravacao);

				      }
					auxiliar = lerArquivo.readLine();

				}
				
				arquivo.close();	
				lerArquivo.close();
			
				
				path.delete();
				copia.renameTo(path);
	
}
	
	
	public  void remocaoPrato(Pratos prato) throws IOException{
		
		

				File path = new File("./doc/pratos.txt");
				BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
				String verifica = prato.getCodigo() + ";" + prato.getNome() + ";" + prato.getQtEstoque()+
						 ";" +prato.getPreco()+ ";" + prato.getNumServe() + "\n";
				
				

				
				File copia = new File("./doc/pratoscopia.txt");
				
				BufferedWriter arquivo;
				arquivo = new BufferedWriter(new FileWriter(copia, true));
	
				String auxiliar = lerArquivo.readLine();
				
				while(auxiliar != null){
					
					if(verifica != auxiliar){
						
						String gravacao = auxiliar;
						arquivo.append(gravacao);

				      }
					auxiliar = lerArquivo.readLine();

				}
				
				arquivo.close();	
				lerArquivo.close();
			
				path.delete();
				copia.renameTo(path);
		
		}
	
	
	public  void remocaoSobremesa(Sobremesa sobremesa) throws IOException{

		

				File path = new File("./doc/sobremesas.txt");
				BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
				String verifica = sobremesa.getCodigo() + ";" + sobremesa.getNome() + ";" + sobremesa.getQtEstoque()+
						 ";" +sobremesa.getPreco()+ "\n";
				

				
				File copia = new File("./doc/sobremesacopia.txt");
				
				BufferedWriter arquivo;
				arquivo = new BufferedWriter(new FileWriter(copia, true));
	
				String auxiliar = lerArquivo.readLine();
				
				while(auxiliar != null){
					
					if(verifica != auxiliar){
						
						String gravacao = auxiliar;
						arquivo.append(gravacao);

				      }
					auxiliar = lerArquivo.readLine();

				}
				
				arquivo.close();	
				lerArquivo.close();
			
				path.delete();
				copia.renameTo(path);
	
	}
	
	//Verificar Ação
	
	public boolean verificarBebida(Bebida bebida) throws IOException {
		
		File path = new File("./doc/bebidas.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
		String verifica =  bebida.getCodigo() + ";" + bebida.getNome() + ";" + bebida.getQtEstoque()+
				 ";" +bebida.getPreco()+ ";" + bebida.getVolume();
		
		String auxiliar = lerArquivo.readLine();
		
		while(auxiliar != null){
			
			if(!verifica.equals(auxiliar)){
				
				auxiliar = lerArquivo.readLine();
		      }
			else
				{
					lerArquivo.close();
					return true;
					
				}

		}
		lerArquivo.close();
		return false;

	}
	
	public boolean verificarPrato(Pratos prato) throws IOException {
		
		File path = new File("./doc/pratos.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
		String verifica = prato.getCodigo() + ";" + prato.getNome() + ";" + prato.getQtEstoque()+
				 ";" +prato.getPreco()+ ";" + prato.getNumServe();
		
		String auxiliar = lerArquivo.readLine();
		
		while(auxiliar != null){
			
			if(!verifica.equals(auxiliar)){
				
				auxiliar = lerArquivo.readLine();
		      }
			else
				{
					lerArquivo.close();
					return true;
					
				}

		}
		lerArquivo.close();
		return false;

	}

	
	public boolean verificarSobremesa(Sobremesa sobremesa) throws IOException {
		
		File path = new File("./doc/sobremesas.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));
		String verifica = sobremesa.getCodigo() + ";" + sobremesa.getNome() + ";" + sobremesa.getQtEstoque()+
				 ";" +sobremesa.getPreco();
		
		String auxiliar = lerArquivo.readLine();
		
		while(auxiliar != null){
			
			if(!verifica.equals(auxiliar)){
				
				auxiliar = lerArquivo.readLine();
		      }
			else
				{
					lerArquivo.close();
					return true;
					
				}

		}
		lerArquivo.close();
		return false;

	}
	
	// Para Todos o Menor Numero Será 5
	public  boolean qtMinimaItem(String codigo, String quantidade) throws IOException{
		
		File path = new File("./doc/sobremesas.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));

		
		String auxiliar = lerArquivo.readLine();
		
		while(auxiliar != null){
			
			if(auxiliar.startsWith(codigo)){
				break;
		      }
			else
				{
					auxiliar = lerArquivo.readLine();
					
				}

		}
		
		String verifica  = new String();
		String vetor[] = new String [10];
		int i = 0,contador = 0;
		char charaux;
		
		for(i=0; i < auxiliar.length(); i++ ){
		
			charaux = auxiliar.charAt(i);
		
			if(charaux == ';' || charaux == '\n' ){
			
				vetor[contador] = verifica;
				verifica  = new String();
				contador += 1;
			}
			else{
				verifica = verifica + charaux;
			}
		}	
		
		int qtEstoque = Integer.valueOf(vetor[2]).intValue();
		int qtRecebida = Integer.valueOf(quantidade).intValue();
		
		if(qtRecebida < 0){
			qtEstoque = (-1 * qtRecebida) + qtEstoque;  
			
			Sobremesa sobre = new Sobremesa();
			sobre.setCodigo(Integer.valueOf(codigo).intValue());
			sobre.setNome(vetor[1]);
			sobre.setQtEstoque(qtEstoque);
			sobre.setPreco(Float.valueOf(vetor[3]));
			adicionarSobremesa(sobre);
			lerArquivo.close();
			return true;
		}
		if(qtRecebida > 0 && (qtEstoque - qtRecebida) <= 5){
			lerArquivo.close();
			return false;
		}
		else{
			Sobremesa sobre = new Sobremesa();
			sobre.setCodigo(Integer.valueOf(codigo).intValue());
			sobre.setNome(vetor[1]);
			sobre.setQtEstoque(qtEstoque);
			sobre.setPreco(Float.valueOf(vetor[3]));
			adicionarSobremesa(sobre);
			lerArquivo.close();
			return true;
		}
	}
	

	public  boolean qtMinimaPrato(String codigo, String quantidade) throws IOException{
		
		File path = new File("./doc/pratos.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));

		
		String auxiliar = lerArquivo.readLine();
		
		while(auxiliar != null){
			
			if(auxiliar.startsWith(codigo)){
				break;
		      }
			else
				{
					auxiliar = lerArquivo.readLine();
					
				}

		}
		
		String verifica  = new String();
		String vetor[] = new String [10];
		int i = 0,contador = 0;
		char charaux;
		
		for(i=0; i < auxiliar.length(); i++ ){
		
			charaux = auxiliar.charAt(i);
		
			if(charaux == ';' || charaux == '\n' ){
			
				vetor[contador] = verifica;
				verifica  = new String();
				contador += 1;
			}
			else{
				verifica = verifica + charaux;
			}
		}	
		
		int qtEstoque = Integer.valueOf(vetor[2]).intValue();
		int qtRecebida = Integer.valueOf(quantidade).intValue();
		
		if(qtRecebida < 0){
			qtEstoque = (-1 * qtRecebida) + qtEstoque;  
			
			Pratos prato = new Pratos();
			prato.setCodigo(Integer.valueOf(codigo).intValue());
			prato.setNome(vetor[1]);
			prato.setQtEstoque(qtEstoque);
			prato.setPreco(Float.valueOf(vetor[3]));
			prato.setNumServe(Integer.valueOf(vetor[4]));
			adicionarPrato(prato);
			lerArquivo.close();
			return true;
		}
		if(qtRecebida > 0 && (qtEstoque - qtRecebida) <= 5){
			lerArquivo.close();
			return false;
		}
		else{
			Pratos prato = new Pratos();
			prato.setCodigo(Integer.valueOf(codigo).intValue());
			prato.setNome(vetor[1]);
			prato.setQtEstoque(qtEstoque);
			prato.setPreco(Float.valueOf(vetor[3]));
			prato.setNumServe(Integer.valueOf(vetor[4]));
			adicionarPrato(prato);
			lerArquivo.close();
			return true;
		}
	}
	public  boolean qtMinimaBebida(String codigo, String quantidade) throws IOException{

		File path = new File("./doc/bebidas.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));

		
		String auxiliar = lerArquivo.readLine();
		
		while(auxiliar != null){
			
			if(auxiliar.startsWith(codigo)){
				break;
		      }
			else
				{
					auxiliar = lerArquivo.readLine();
					
				}

		}
		
		String verifica  = new String();
		String vetor[] = new String [10];
		int i = 0,contador = 0;
		char charaux;
		
		for(i=0; i < auxiliar.length(); i++ ){
		
			charaux = auxiliar.charAt(i);
		
			if(charaux == ';' || charaux == '\n' ){
			
				vetor[contador] = verifica;
				verifica  = new String();
				contador += 1;
			}
			else{
				verifica = verifica + charaux;
			}
		}	
		
		int qtEstoque = Integer.valueOf(vetor[2]).intValue();
		int qtRecebida = Integer.valueOf(quantidade).intValue();
		
		if(qtRecebida < 0){
			qtEstoque = (-1 * qtRecebida) + qtEstoque;  
			
			Bebida bebida = new Bebida();
			bebida.setCodigo(Integer.valueOf(codigo).intValue());
			bebida.setNome(vetor[1]);
			bebida.setQtEstoque(qtEstoque);
			bebida.setPreco(Float.valueOf(vetor[3]));
			bebida.setVolume(vetor[4]);
			adicionarBebida(bebida);
			lerArquivo.close();
			return true;
		}
		if(qtRecebida > 0 && (qtEstoque - qtRecebida) <= 5){
			lerArquivo.close();
			return false;
		}
		else{
			Bebida bebida = new Bebida();
			bebida.setCodigo(Integer.valueOf(codigo).intValue());
			bebida.setNome(vetor[1]);
			bebida.setQtEstoque(qtEstoque);
			bebida.setPreco(Float.valueOf(vetor[3]));
			bebida.setVolume(vetor[4]);
			adicionarBebida(bebida);
			lerArquivo.close();
			return true;
		}		
	}

	public Bebida receberBebida(String codigo,String quantidade) throws IOException{

		File path = new File("./doc/bebidas.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));

		
		String auxiliar = lerArquivo.readLine();
		
		while(auxiliar != null){
			
			if(auxiliar.startsWith(codigo)){
				break;
		      }
			else
				{
					auxiliar = lerArquivo.readLine();
					
				}

		}
		
		String verifica  = new String();
		String vetor[] = new String [10];
		int i = 0,contador = 0;
		char charaux;
		
		for(i=0; i < auxiliar.length(); i++ ){
		
			charaux = auxiliar.charAt(i);
		
			if(charaux == ';' || charaux == '\n' ){
			
				vetor[contador] = verifica;
				verifica  = new String();
				contador += 1;
			}
			else{
				verifica = verifica + charaux;
			}
		}	

			Bebida bebida = new Bebida();
			bebida.setCodigo(Integer.valueOf(codigo).intValue());
			bebida.setNome(vetor[1]);
			bebida.setQtEstoque(Integer.valueOf(quantidade).intValue());
			bebida.setPreco(Float.valueOf(vetor[3]));
			bebida.setVolume(vetor[4]);
			lerArquivo.close();
			
			return bebida;
	}

	public Pratos receberPrato(String codigo,String quantidade) throws IOException{
	
		
		File path = new File("./doc/pratos.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));

		
		String auxiliar = lerArquivo.readLine();
		
		while(auxiliar != null){
			
			if(auxiliar.startsWith(codigo)){
				break;
		      }
			else
				{
					auxiliar = lerArquivo.readLine();
					
				}

		}
		
		String verifica  = new String();
		String vetor[] = new String [10];
		int i = 0,contador = 0;
		char charaux;
		
		for(i=0; i < auxiliar.length(); i++ ){
		
			charaux = auxiliar.charAt(i);
		
			if(charaux == ';' || charaux == '\n' ){
			
				vetor[contador] = verifica;
				verifica  = new String();
				contador += 1;
			}
			else{
				verifica = verifica + charaux;
			}
		}	
		
		
			
			Pratos prato = new Pratos();
			prato.setCodigo(Integer.valueOf(codigo).intValue());
			prato.setNome(vetor[1]);
			prato.setQtEstoque(Integer.valueOf(quantidade));
			prato.setPreco(Float.valueOf(vetor[3]));
			prato.setNumServe(Integer.valueOf(vetor[4]));
			adicionarPrato(prato);
			lerArquivo.close();
			return prato;
	}

	public Sobremesa receberSobremesa(String codigo,String quantidade) throws IOException{
		
		File path = new File("./doc/sobremesas.txt");
		BufferedReader lerArquivo = new BufferedReader(new FileReader(path));

		
		String auxiliar = lerArquivo.readLine();
		
		while(auxiliar != null){
			
			if(auxiliar.startsWith(codigo)){
				break;
		      }
			else
				{
					auxiliar = lerArquivo.readLine();
					
				}

		}
		
		String verifica  = new String();
		String vetor[] = new String [10];
		int i = 0,contador = 0;
		char charaux;
		
		for(i=0; i < auxiliar.length(); i++ ){
		
			charaux = auxiliar.charAt(i);
		
			if(charaux == ';' || charaux == '\n' ){
			
				vetor[contador] = verifica;
				verifica  = new String();
				contador += 1;
			}
			else{
				verifica = verifica + charaux;
			}
		}	
		
			
			Sobremesa sobre = new Sobremesa();
			sobre.setCodigo(Integer.valueOf(codigo).intValue());
			sobre.setNome(vetor[1]);
			sobre.setQtEstoque(Integer.valueOf(quantidade));
			sobre.setPreco(Float.valueOf(vetor[3]));
			adicionarSobremesa(sobre);
			lerArquivo.close();
			return sobre;
	}

	public void mostrarBebida() throws IOException{


		File path = new File("./doc/sobremesas.txt");

		Desktop desk = new Desktop(path);

	}


}