package Model;

public class Produtos {

	private int codigo;
	private String nome;
	private int qtEstoque;
	private double preco;
	
	public Produtos(){
		
		setCodigo(000);
		setNome("");
		setQtEstoque(0);
		setPreco(0.00);
	}
	
	
	public String getNome() {
		return nome;
	   }
	public void setNome(String nome) {
		this.nome = nome;
    	}
	
	public int getQtEstoque() {
		return qtEstoque;
	   }
	public void setQtEstoque(int qtEstoque) {
		this.qtEstoque = qtEstoque;
	   }
	
	public double getPreco() {
		return preco;
	   }
	public void setPreco(double preco) {
		this.preco = preco;
	   }


	public int getCodigo() {
		return codigo;
	}


	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	

}
