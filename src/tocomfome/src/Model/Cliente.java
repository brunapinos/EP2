package Model;

public class Cliente {

	private String nome;
	private String telefone;
	private boolean necessidadeEndereco;
	private String endereco;
	private boolean reservaMesa;
	
	public Cliente(){
		setNome("nulo");
		setTelefone("00000000");
		setNecessidadeEndereco(false);
		setEndereco("nulo");
		setReservaMesa(false);
		
	}
	
	public Cliente(String nome,String telefone,boolean necessidadeEndereco,String endereco,boolean reservaMesa)
	{
		setNome(nome);
		setTelefone(telefone);
		setNecessidadeEndereco(necessidadeEndereco);
		setEndereco(endereco);
		setReservaMesa(reservaMesa);
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public boolean isReservaMesa() {
		return reservaMesa;
	}

	public void setReservaMesa(boolean reservaMesa) {
		this.reservaMesa = reservaMesa;
	}
	
	public boolean isNecessidadeEndereco() {
		return necessidadeEndereco;
	}
	
	public void setNecessidadeEndereco(boolean necessidadeEndereco) {
		this.necessidadeEndereco = necessidadeEndereco;
	}
	

	
	
}
