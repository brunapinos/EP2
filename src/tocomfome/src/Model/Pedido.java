package Model;



public class Pedido {

	private double precoTotal;
	private Produtos [] produtos;
	private int [] quantidade;
	private String observacao;
	private boolean pago;
	private String tipoPagamento;
	
	public double getPrecoTotal() {
		return precoTotal;
	}
	public void setPrecoTotal(double precoTotal) {
		this.precoTotal = precoTotal;
	}
	public Produtos[] getProdutos() {
		return produtos;
	}
	
	public void setProdutos(int tamanho) {
		this.produtos = new Produtos[tamanho];
	}
	
	public void setProdutos(Produtos[] produtos) {
		this.produtos = produtos;
	}
	public int[] getQuantidade() {
		return quantidade;
	}
	
	public void setQuantidade(int tamanho) {
		this.quantidade = new int [tamanho];
	}
	
	public void setQuantidade(int[] quantidade) {
		this.quantidade = quantidade;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;}



	public boolean isPago() {
		return pago;
	}



	public void setPago(boolean pago) {
		this.pago = pago;
	}



	public String getTipoPagamento() {
		return tipoPagamento;
	}



	public void setTipoPagamento(String tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}
}
