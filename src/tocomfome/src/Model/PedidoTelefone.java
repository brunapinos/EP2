package Model;

public class PedidoTelefone extends Pedido{

	private Cliente cliente;
	
	public PedidoTelefone(){
		setPrecoTotal(0.00);
		setObservacao("nulo");
		setPago(false);
		setTipoPagamento("nulo");
	}
	
	public PedidoTelefone(Cliente cliente,Produtos []produto, int []quantidade){
		setPrecoTotal(0.00);
		setObservacao("");
		setPago(false);
		setTipoPagamento("");
	}
	
	
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}
