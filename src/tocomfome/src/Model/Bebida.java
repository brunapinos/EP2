package Model;


public class Bebida extends Produtos {

	private String volume;

	public Bebida(){
		setCodigo(000);
		setNome("nulo");
		setQtEstoque(0);
		setPreco(0.00);
		setVolume("nulo");
	}

	public Bebida(int codigo, String nome, int qtEstoque, double preco, String volume) {
		
		setCodigo(codigo);
		setNome(nome);
		setQtEstoque(qtEstoque);
		setPreco(preco);
		setVolume(volume);
		
		
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}
	
	
}
