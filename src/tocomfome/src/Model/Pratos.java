package Model;

public class Pratos extends Produtos{

	private int numServe;
	
	public Pratos(){
		
		setNome("nulo");
		setQtEstoque(0);
		setPreco(0.00);
		setNumServe(0);
		 
	}

	public Pratos(int codigo, String nome, int qtEstoque, double preco, int numServe) {
		
		setCodigo(codigo);
		setNome(nome);
		setQtEstoque(qtEstoque);
		setPreco(preco);
		setNumServe(numServe);
		
		
	}

	public int getNumServe() {
		return numServe;
	}

	public void setNumServe(int numServe) {
		this.numServe = numServe;
	}
	
}
