package Model;

public class Mesa {

	private int qtMesa;
	private String[] mesas;
	
	public Mesa(){
		setQtMesa(20); // quantidade de mesas no Restaurante
	}

	public int getQtMesa() {
		return qtMesa;
	}

	public void setQtMesa(int qtMesa) {
		this.qtMesa = qtMesa;
	}
	
	public void setMesas(int qtMesa) {
		
		String[] qtmesa = new String[qtMesa];
		for(int j = 0; j <= qtMesa; j++){
		
			qtmesa[qtMesa] = String.valueOf(qtMesa);
			}
		this.mesas = qtmesa; 
	}
	
	
	public String[] getMesas() {
		return mesas;
	}
}
