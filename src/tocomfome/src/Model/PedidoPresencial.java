package Model;

public class PedidoPresencial extends Pedido {
	
	private int mesa;

	
	public PedidoPresencial(){
		setMesa(0);
		setPrecoTotal(0.00);
		setObservacao("");
		setPago(false);
		setTipoPagamento("");
		
	}
	
	public PedidoPresencial(Produtos []produto, int []quantidade){
		setMesa(0);
		setPrecoTotal(0.00);
		setProdutos(produto);
		setQuantidade(quantidade);
		setObservacao("");
		setPago(false);
		setTipoPagamento("");
	}
	
	public int getMesa() {
		return mesa;
	}
	public void setMesa(int mesa) {
		this.mesa = mesa;
	}
	
}

