package Teste;

import java.io.IOException;

import org.junit.Test;

import Controller.CadastrarEstoque;
import Controller.CadastrarMesa;
import Teste.PedidoPresencialTeste;
import Model.Produtos;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class TesteCadastroPedido {

	@Test
	public void deveCadastrarPedido() throws IOException {
	
		// Preparacao
		PratoTeste produto1 = new PratoTeste(001,"comida",4,10.0,2);
		PratoTeste produto2 = new PratoTeste(003,"comida2",4,20.0,3);
		
		CadastrarEstoque teste = new CadastrarEstoque();
		teste.adicionarPrato(produto1);
		teste.adicionarPrato(produto2);
		
		//Teste
		
		Produtos[] produto = new Produtos [2];
		
		produto[0] = produto1;
		produto[1] = produto2;
		
		
		int quantidades[] = new int [2];
		quantidades[0] = 3;
		quantidades[1] = 4;

		
		PedidoPresencialTeste pedido = new PedidoPresencialTeste();

		pedido.setProdutos(2);
		pedido.setQuantidade(2);
		pedido.setProdutos(produto);
		pedido.setQuantidade(quantidades);
		
		
		CadastrarMesa mesa = new CadastrarMesa();
		mesa.salvarPedido(pedido);
		
		
		// Confirmação
		
		assertFalse(mesa.verificaPedido(pedido));
		
		
	}
	
	@Test
	public void deveRemoverPedido() throws IOException {
	
		// Preparacao
		PratoTeste produto1 = new PratoTeste(001,"comida",4,10.0,2);
		PratoTeste produto2 = new PratoTeste(003,"comida2",4,20.0,3);
		
		CadastrarEstoque teste = new CadastrarEstoque();
		teste.adicionarPrato(produto1);
		teste.adicionarPrato(produto2);
		
		Produtos[] produto = new Produtos [2];
		
		produto[0] = produto1;
		produto[1] = produto2;
		
		
		int quantidades[] = new int [2];
		quantidades[0] = 3;
		quantidades[1] = 4;

		
		PedidoPresencialTeste pedido = new PedidoPresencialTeste();

		pedido.setProdutos(2);
		pedido.setQuantidade(2);
		pedido.setProdutos(produto);
		pedido.setQuantidade(quantidades);
		
		
		CadastrarMesa mesa = new CadastrarMesa();
		mesa.salvarPedido(pedido);
		
		//Teste
		quantidades[1] = 3;
		pedido.setQuantidade(quantidades);
		mesa.removerPedido(pedido);
		mesa.salvarPedido(pedido);
		
		
		// Confirmação
		
		assertFalse(mesa.verificaPedido(pedido));
		
		
	}
	


}
