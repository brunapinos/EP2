package Teste;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import Controller.CadastrarEstoque;
import Controller.CadastrarFuncionario;

public class TesteCadastroEstoque {


	@Test
	public void deveCadastrarEstoque() throws Exception{

		// Preparação
		BebidaTeste bebida = new BebidaTeste(000, "oi", 1, 10.0,"1L");
		CadastrarEstoque teste = new CadastrarEstoque();
		
		PratoTeste prato = new PratoTeste(000, "oi", 1, 10.0,2);
		SobremesaTeste sobre = new SobremesaTeste(000, "oi", 1, 10.0);

		// Teste 
		teste.adicionarBebida(bebida);
		teste.adicionarPrato(prato);
		teste.adicionarSobremesa(sobre);
		
		//Verifica
		
		assertFalse(teste.verificarBebida(bebida));
		assertFalse(teste.verificarPrato(prato));
		assertFalse(teste.verificarSobremesa(sobre));
	
		
}
	
	@Test
	public void deveRemoverEstoque() throws Exception{
		

		// Preparação
		BebidaTeste bebida = new BebidaTeste(000, "oi", 1, 10.0,"1L");
		CadastrarEstoque teste = new CadastrarEstoque();
		
		PratoTeste prato = new PratoTeste(000, "oi", 1, 10.0,2);
		SobremesaTeste sobre = new SobremesaTeste(000, "oi", 1, 10.0);

		teste.adicionarBebida(bebida);
		teste.adicionarPrato(prato);
		teste.adicionarSobremesa(sobre);

		// Teste 
		
		teste.remocaoBebida(bebida);
		teste.remocaoPrato(prato);
		teste.remocaoSobremesa(sobre);
		
		//Verifica
		
		assertFalse(teste.verificarBebida(bebida) && teste.verificarPrato(prato) && teste.verificarSobremesa(sobre));

				
		}

	@Test
	public void deveAlterarEstoque() throws Exception{

		

		// Preparação
		BebidaTeste bebida = new BebidaTeste(000, "oi", 1, 10.0,"1L");
		CadastrarEstoque teste = new CadastrarEstoque();
		
		PratoTeste prato = new PratoTeste(000, "oi", 1, 10.0,2);
		SobremesaTeste sobre = new SobremesaTeste(000, "oi", 1, 10.0);

		teste.adicionarBebida(bebida);
		teste.adicionarPrato(prato);
		teste.adicionarSobremesa(sobre);

		// Teste 
		
		bebida.setPreco(200.0);
		prato.setQtEstoque(13);
		sobre.setQtEstoque(12);
		
		teste.alteracaoBebida(bebida);
		teste.alteracaoPrato(prato);
		teste.alteracaoSobremesa(sobre);
		
		//Verifica
		
		assertFalse(teste.verificarBebida(bebida) && teste.verificarPrato(prato) && teste.verificarSobremesa(sobre));
		
}

	
}
