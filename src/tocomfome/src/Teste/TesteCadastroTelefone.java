package Teste;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;

import java.io.IOException;

import org.junit.Test;

import Model.Produtos;

import Controller.CadastrarEstoque;
import Controller.CadastrarPedidoTelefone;

public class TesteCadastroTelefone {


	@Test
	public void deveAlterarCliente() throws IOException{
		
		ClienteTeste cliente = new ClienteTeste("Bruno","99999999",false,"Rua 5 casa 2",true);
		
		CadastrarPedidoTelefone teste = new CadastrarPedidoTelefone();
		
		teste.alterarCliente(cliente);
		
		assertTrue(teste.verificarCliente(cliente.getTelefone()));
		
	}
	
	@Test
	public void deveCadastrarPedido() throws IOException{
		
		
		PratoTeste produto1 = new PratoTeste(001,"comida",4,10.0,2);
		PratoTeste produto2 = new PratoTeste(003,"comida2",4,20.0,3);
		
		CadastrarEstoque teste = new CadastrarEstoque();
		teste.adicionarPrato(produto1);
		teste.adicionarPrato(produto2);	
		
		Produtos[] produto = new Produtos [2];
		
		produto[0] = produto1;
		produto[1] = produto2;
		
		
		int quantidades[] = new int [2];
		quantidades[0] = 3;
		quantidades[1] = 4;
		
		ClienteTeste cliente = new ClienteTeste("Bruno","99999999",false,"Rua 5 casa 2",true);
		
		PedidoTelefoneTeste pedido = new PedidoTelefoneTeste();

		pedido.setCliente(cliente);
		pedido.setProdutos(2);
		pedido.setQuantidade(2);
		pedido.setProdutos(produto);
		pedido.setQuantidade(quantidades);
		
		//Teste
		
		CadastrarPedidoTelefone teste1 = new CadastrarPedidoTelefone();


		teste1.salvarPedido(pedido);
		
		
		assertFalse(teste1.verificarPedido(pedido));
		
	}
}
