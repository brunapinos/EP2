package Teste;

import static org.junit.Assert.*;
import org.junit.Test;

import Controller.CadastrarFuncionario;


public class TesteCadastroFuncionario {

	
	@Test
	public void deveCadastrarFuncionario() throws Exception{

		// Preparação
		FuncionarioTeste funcionario = new FuncionarioTeste("oi", "oi");
		CadastrarFuncionario teste = new CadastrarFuncionario();
		
		// Teste 
		teste.criacaoArquivo(funcionario);
		
		//Verifica
		
		assertFalse(teste.verificarItem(funcionario));
		
}
	
	@Test
	public void deveRemoverFuncionario() throws Exception{
		
		//Preparação
		FuncionarioTeste funcionario = new FuncionarioTeste("oi", "oi");
		CadastrarFuncionario teste = new CadastrarFuncionario();
		teste.criacaoArquivo(funcionario);
		
		// Teste 
				teste.remocaoItem(funcionario);
				
				//Verifica
				
				assertFalse(teste.verificarItem(funcionario));
				
		}
	}

