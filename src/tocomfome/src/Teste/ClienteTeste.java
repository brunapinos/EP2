package Teste;

import Model.Cliente;

public class ClienteTeste extends Cliente{

	public ClienteTeste(String nome,String telefone,boolean necessidadeEndereco,String endereco,boolean reservaMesa) {
		
		setNome(nome);
		setTelefone(telefone);
		setNecessidadeEndereco(necessidadeEndereco);
		setEndereco(endereco);
		setReservaMesa(reservaMesa);
	}

}
