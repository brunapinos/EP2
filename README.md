# EP1 - OO (UnB - Gama)

# Orientação a Objetos 2/2016

# Bruna Pinos 15/0119984

# Programa para controle de Vendas e Estoque


** Este programa controla o estoque e vendas de um restaurante.

# Faça questão de clonar todo o repositório em uma pasta de fácil acesso.

# Como compilar e executar

	*Este programa roda no sistema operacional Linux SOMENTE;
	* Baixe o aplicativo Eclipse, caso não tenha instalado;
	* Após baixar, abra o arquivo "Main", que se encontra dentro da pasta do Diretório dentro de outra pasta "Controller";
	* Clique run;
	
# Funcionamento

	*Ao ser executado a primeira janela que aparece é a do login;
	*Clique em cadastrar login para cadastrar usuários e senhas;
	*Caso já tenha cadastrado basta digitar as informações e clicar Entrar;
	*Assim que entrar, terá o botão Estoque onde se encontra todas as funcionalidades de estoque e,
	o botão Pedidos que possui todas as funcionalidades de pedidos;
	*A qualquer momento poderá mudar de tela, basta clicar no botão Usuário na barra de menu;
	* O cardápio pode ser acessado a qualquer momento;
